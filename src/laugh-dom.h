/*
 * Laugh.
 *
 * An glib SMIL library.
 *
 * Authored By Koos Vriezen  <koos.vriezen@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef _LAUGH_DOM_H
#define _LAUGH_DOM_H

#include <clutter/clutter-container.h>
#include <glib-object.h>
#include <glib.h>

G_BEGIN_DECLS

#define LAUGH_TYPE_NODE laugh_node_get_type()

#define LAUGH_NODE(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  LAUGH_TYPE_NODE, LaughNode))

#define LAUGH_TYPE_NODE_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  LAUGH_TYPE_NODE, LaughNodeClass))

#define LAUGH_IS_NODE(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  LAUGH_TYPE_NODE))

#define LAUGH_IS_NODE_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  LAUGH_TYPE_NODE))

#define LAUGH_NODE_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  LAUGH_TYPE_NODE, LaughNodeClass))

#define LAUGH_TYPE_DOCUMENT laugh_document_get_type()

#define LAUGH_DOCUMENT(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  LAUGH_TYPE_DOCUMENT, LaughDocument))

#define LAUGH_TYPE_DOCUMENT_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  LAUGH_TYPE_DOCUMENT, LaughDocumentClass))

#define LAUGH_IS_DOCUMENT(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  LAUGH_TYPE_DOCUMENT))

#define LAUGH_IS_DOCUMENT_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  LAUGH_TYPE_DOCUMENT))

#define LAUGH_DOCUMENT_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  LAUGH_TYPE_DOCUMENT, LaughDocumentClass))

typedef struct _LaughNode        LaughNode;
typedef struct _LaughNodeClass   LaughNodeClass;
typedef struct _LaughNodePrivate LaughNodePrivate;
typedef struct _LaughDocument        LaughDocument;
typedef struct _LaughDocumentClass   LaughDocumentClass;
typedef struct _LaughDocumentPrivate LaughDocumentPrivate;
typedef struct _LaughInitializer LaughInitializer;
typedef struct _LaughRole LaughRole;
typedef enum _LaughNodeState LaughNodeState;
typedef enum _LaughNodeTagId LaughNodeTagId;
typedef enum _LaughNodeAttributeId LaughNodeAttributeId;

enum _LaughNodeState {
    LaughStateInit, LaughStateInitialized,
    LaughStateBegun, LaughStatePaused,
    LaughStateStopped, LaughStateFreezed
};

enum _LaughNodeTagId {
    LaughTagIdDocument = 1,
    LaughTagIdSmil,
    LaughTagIdHead,
    LaughTagIdLayout,
    LaughTagIdRootLayout,
    LaughTagIdRegion,
    LaughTagIdBody,
    LaughTagIdSeq,
    LaughTagIdPar,
    LaughTagIdExcl,
    LaughTagIdSwitch,
    LaughTagIdAudio,
    LaughTagIdBrush,
    LaughTagIdImg,
    LaughTagIdRef,
    LaughTagIdText,
    LaughTagIdVideo,
    LaughTagIdSet,
    LaughTagIdAnimate,
    LaughTagIdA,
    LaughTagIdAnimateMotion,
    LaughTagIdAnchor,
    LaughTagIdArea,
    LaughTagIdLast
};

enum _LaughNodeAttributeId {
    LaughAttrIdId = 1,
    LaughAttrIdName,
    LaughAttrIdLeft,
    LaughAttrIdTop,
    LaughAttrIdWidth,
    LaughAttrIdHeight,
    LaughAttrIdRight,
    LaughAttrIdBottom,
    LaughAttrIdBgColor,
    LaughAttrIdBgColor1,
    LaughAttrIdBgImage,
    LaughAttrIdShowBg,
    LaughAttrIdZIndex,
    LaughAttrIdBegin,
    LaughAttrIdDur,
    LaughAttrIdEnd,
    LaughAttrIdEndSync,
    LaughAttrIdRepeat,
    LaughAttrIdSrc,
    LaughAttrIdRegion,
    LaughAttrIdType,
    LaughAttrIdSubType,
    LaughAttrIdFill,
    LaughAttrIdFit,
    LaughAttrIdPeers,
    LaughAttrIdHigher,
    LaughAttrIdLower,
    LaughAttrIdPauseDisplay,
    LaughAttrIdHRef,
    LaughAttrIdCoord,
    LaughAttrIdTransIn,
    LaughAttrIdTransOut,
    LaughAttrIdSystemBitrate,
    LaughAttrIdSensitivity,
    LaughAttrIdFontColor,
    LaughAttrIdFontSize,
    LaughAttrIdHAlign,
    LaughAttrIdVAlign,
    LaughAttrIdCharset,
    LaughAttrIdBgOpacity,
    LaughAttrIdTargetElement,
    LaughAttrIdTarget,
    LaughAttrIdAttr,
    LaughAttrIdAttrName,
    LaughAttrIdTo,
    LaughAttrIdChangeBy,
    LaughAttrIdFrom,
    LaughAttrIdValues,
    LaughAttrIdCalcMode,
    LaughAttrIdKeyTimes,
    LaughAttrIdKeySplines,
    LaughAttrIdLast
};

typedef
enum _LaughRoleType
{
    LaughRoleTypeDisplay,
    LaughRoleTypeDisplayGroup,
    LaughRoleTypeMedia,
    LaughRoleTypeTiming
} LaughRoleType;

struct _LaughRole
{
    LaughRoleType type;
};

struct _LaughNode
{
    GObject parent;

    /*< public >*/
    LaughNodeTagId id;
    guint element_id;
    LaughNode *parent_node;
    LaughNode *previous_sibling;
    LaughNode *next_sibling;
    LaughNode *first_child;
    LaughNode *last_child;
    LaughDocument *document;
    GHashTable *attributes;
    LaughNodeState state;

    /*< private >*/
    LaughNodePrivate *priv;
};

struct _LaughNodeClass
{
    GObjectClass parent_class;

    /*< public >*/
    void (*set_attribute) (LaughNode *laugh_node,
            LaughNodeAttributeId att, const gchar *val, gpointer *undo);
    const gchar *(*get_attribute) (LaughNode *laugh_node,
            LaughNodeAttributeId att);
    void (*init) (LaughNode *laugh_node, LaughInitializer *initializer);
    void (*start) (LaughNode *laugh_node);
    void (*pause) (LaughNode *laugh_node);
    void (*stop) (LaughNode *laugh_node);
    void (*freeze) (LaughNode *laugh_node);
    LaughRole *(*role) (LaughNode *laugh_node, LaughRoleType type);

    /* event signals */
    void (*initialized) (LaughNode *laugh_node);
    void (*started) (LaughNode *laugh_node);
    void (*stopped) (LaughNode *laugh_node);
};

#include "laugh-timing.h"

struct _LaughInitializer
{
    LaughNode *node;
    ClutterContainer *parent_region;
    LaughRoleTiming *parent_segment;
    guint parent_width;
    guint parent_height;
    GSList *init_pending;
    gulong init_signal;
    void (*initialized) (LaughNode *node, LaughInitializer *initializer);
};

struct _LaughDocument
{
    LaughNode parent;

    /*< public >*/
    LaughRoleTiming *timing;

    /*< private >*/
    LaughDocumentPrivate *priv;
};

struct _LaughDocumentClass
{
    LaughNodeClass parent_class;
};

GType laugh_node_get_type (void) G_GNUC_CONST;

GType laugh_document_get_type (void) G_GNUC_CONST;

LaughNodeTagId laugh_tag_from_string (const gchar *tag);

const gchar *laugh_tag_from_id (LaughNodeTagId id);

LaughNodeAttributeId laugh_attribute_from_string (const gchar *tag);

const gchar *laugh_attribute_from_id (LaughNodeAttributeId id);

/* constructors */
LaughInitializer *laugh_dom_initializer_new (ClutterContainer *parent,
                                             guint width, guint height);

LaughNode *laugh_node_new (LaughDocument *document, LaughNodeTagId id,
                           GHashTable *attributes);

LaughDocument *laugh_document_new (const gchar *uri);

/* sub classing */
void laugh_node_base_construct (LaughDocument *document, LaughNode *node,
                LaughNodeTagId id, GHashTable *attributes);

void laugh_node_base_emit_initialized (LaughNode *node);

/* attributes */
const gchar *laugh_node_get_attribute (LaughNode *node,
        LaughNodeAttributeId attribute);

void laugh_node_set_attribute (LaughNode *node,
        LaughNodeAttributeId attr, const gchar *val, gpointer *undo);

void laugh_attributes_set (gpointer key, gpointer val, gpointer data);

/* (de)serializing */
void laugh_node_set_inner_xml (LaughNode *node, const gchar *xml);

gchar *laugh_node_get_inner_xml (LaughNode *node);

/* tree manipulation */
void laugh_node_append_child (LaughNode *node, LaughNode *child);

void laugh_node_insert_before (LaughNode *node, LaughNode *child, LaughNode *n);

void laugh_node_remove_child (LaughNode *node, LaughNode *child);

/* state manipulation */
void laugh_node_init (LaughNode *node, LaughInitializer *initializer);

void laugh_node_start (LaughNode *node);

void laugh_node_pause (LaughNode *node);

void laugh_node_stop (LaughNode *node);

void laugh_node_freeze (LaughNode *node);

/* searching */
guint laugh_document_id_from_string (LaughDocument *doc, const gchar *str);

LaughNode *laugh_document_get_element_by_id (LaughDocument *doc, guint id);

const gchar *laugh_document_get_uri (LaughDocument *doc);

LaughRole *laugh_node_role_get (LaughNode *laugh_node, LaughRoleType type);

void laugh_document_set_uri (LaughDocument *doc, const gchar *uri);

gboolean
laugh_document_open (LaughDocument *doc, ClutterContainer *parent, gint w, gint h);

G_END_DECLS

#endif
