/*
 * Laugh.
 *
 * An glib SMIL library.
 *
 * Authored By Koos Vriezen  <koos.vriezen@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef _LAUGH_MEDIA_H
#define _LAUGH_MEDIA_H

#include "laugh-dom.h"

G_BEGIN_DECLS

#define LAUGH_TYPE_MEDIA laugh_media_get_type()

#define LAUGH_MEDIA(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  LAUGH_TYPE_MEDIA, LaughMedia))

#define LAUGH_TYPE_MEDIA_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  LAUGH_TYPE_MEDIA, LaughMediaClass))

#define LAUGH_IS_MEDIA(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  LAUGH_TYPE_MEDIA))

#define LAUGH_IS_MEDIA_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  LAUGH_TYPE_MEDIA))

#define LAUGH_MEDIA_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  LAUGH_TYPE_MEDIA, LaughMediaClass))

typedef struct _LaughMedia        LaughMedia;
typedef struct _LaughMediaClass   LaughMediaClass;
typedef struct _LaughMediaPrivate LaughMediaPrivate;

struct _LaughMedia
{
    LaughNode parent;

    /*< private >*/
    LaughMediaPrivate *priv;
};

struct _LaughMediaClass
{
    LaughNodeClass parent_class;

    void (*activated) (LaughNode *node, gint x, gint y);
};

GType laugh_media_get_type (void) G_GNUC_CONST;

LaughNode *laugh_media_new (LaughDocument *doc, LaughNodeTagId id,
                                       GHashTable *attributes);

G_END_DECLS

#endif
