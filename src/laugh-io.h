/*
 * Laugh.
 *
 * An glib SMIL library.
 *
 * Authored By Koos Vriezen  <koos.vriezen@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef _LAUGH_IO_H
#define _LAUGH_IO_H

#include <glib-object.h>

G_BEGIN_DECLS

#define LAUGH_TYPE_IO laugh_io_get_type()

#define LAUGH_IO(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  LAUGH_TYPE_IO, LaughIO))

#define LAUGH_TYPE_IO_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  LAUGH_TYPE_IO, LaughIOClass))

#define LAUGH_IS_IO(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  LAUGH_TYPE_IO))

#define LAUGH_IS_IO_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  LAUGH_TYPE_IO))

#define LAUGH_IO_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  LAUGH_TYPE_IO, LaughIOClass))

typedef struct _LaughIO        LaughIO;
typedef struct _LaughIOClass   LaughIOClass;
typedef struct _LaughIOPrivate LaughIOPrivate;

struct _LaughIO
{
    GObject parent;

    /*< private >*/
    LaughIOPrivate *priv;
};

struct _LaughIOClass
{
    GObjectClass parent_class;

    /*< public >*/
    void (*mime_type) (LaughIO *laugh_io, const gchar *mime);
    void (*completed) (LaughIO *laugh_io, gsize sz, const void *data);
};

GType laugh_io_get_type (void) G_GNUC_CONST;

LaughIO *laugh_io_new (const gchar *uri, const gchar *relative_to);

gboolean laugh_io_open (LaughIO *io);

void laugh_io_cancel (LaughIO *io);

const gchar *laugh_io_get_uri (LaughIO *io);

gchar *laugh_io_uri_resolve (const gchar *uri, const gchar *relative_to);

G_END_DECLS

#endif
