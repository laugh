/*
 * Laugh.
 *
 * An glib SMIL library.
 *
 * Authored By Koos Vriezen  <koos.vriezen@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <string.h>
#include <glib/gprintf.h>

#include "laugh-animate.h"

#define LAUGH_ANIMATE_GET_PRIVATE(obj) \
(G_TYPE_INSTANCE_GET_PRIVATE ((obj), LAUGH_TYPE_ANIMATE, LaughAnimatePrivate))


struct _LaughAnimatePrivate
{
    LaughRoleTiming *timing_role;
    const char *changed_attribute;
    LaughNode *target;
    LaughNodeAttributeId attribute;
};

static gpointer laugh_animate_parent_class = ((void *)0);

static void laugh_animate_finalize (GObject *object)
{
    G_OBJECT_CLASS (laugh_animate_parent_class)->finalize (object);
}

static void laugh_animate_dispose (GObject *object)
{
    LaughAnimate *self = LAUGH_ANIMATE(object);
    LaughRoleTiming *role = self->priv->timing_role;

    G_OBJECT_CLASS (laugh_animate_parent_class)->dispose (object);

    laugh_timing_role_delete (role);
}

static void
_laugh_animate_init (LaughNode *node, LaughInitializer *initializer)
{
    LaughAnimate *self = (LaughAnimate *) node;
    LaughNode *child;
    LaughRoleTiming *parent_segment = initializer->parent_segment;

    if (node->attributes)
        g_hash_table_foreach (node->attributes, laugh_attributes_set, node);

    laugh_timing_role_child_add(initializer->parent_segment, self->priv->timing_role);

    initializer->parent_segment = self->priv->timing_role;

    for (child = node->first_child; child; child = child->next_sibling)
        laugh_node_init (child, initializer);

    initializer->parent_segment = parent_segment;
}

static void _laugh_animate_set_attribute (LaughNode *node,
        LaughNodeAttributeId att, const gchar *value, gpointer *undo)
{
    const gchar *val = value;
    LaughAnimate *self = (LaughAnimate *)node;

    LAUGH_TYPE_NODE_CLASS(laugh_animate_parent_class)->
        set_attribute(node, att, val, undo);
    g_printf ("_laugh_animate_set_attribute %s=%s\n", laugh_attribute_from_id (att), val);

    if (!value && undo)
        val = *(const gchar **)undo;

    laugh_timing_setting_set_attribute (self->priv->timing_role, att, val);
}

static void _laugh_animate_target_destroyed (gpointer data, GObject *obj)
{
    LaughAnimatePrivate *priv = (LaughAnimatePrivate *) data;
    (void) obj;

    priv->target = NULL;
}

static void _laugh_animate_start (LaughNode *node)
{
    LaughAnimate *self = (LaughAnimate *)node;
    LaughAnimatePrivate *priv = self->priv;
    const gchar *elm = laugh_node_get_attribute(node, LaughAttrIdTargetElement);
    const gchar *to = laugh_node_get_attribute (node, LaughAttrIdTo);

    if (!elm)
        elm = laugh_node_get_attribute (node, LaughAttrIdTarget);
    if (!elm) {
        g_printerr ("start '%s' no target\n", laugh_tag_from_id (node->id));
        return;
    }

    priv->target = laugh_document_get_element_by_id (node->document,
            laugh_document_id_from_string (node->document, elm));
    if (!priv->target) {
        g_printerr ("start '%s' element \"%s\" not found\n",
                laugh_tag_from_id (node->id), elm);
        return;
    }
    g_object_weak_ref ((GObject *) priv->target,
            _laugh_animate_target_destroyed, priv);

    switch (node->id) {
        case LaughTagIdSet: {
            const gchar *att = laugh_node_get_attribute (node, LaughAttrIdAttr);

            if (!att)
                att = laugh_node_get_attribute (node, LaughAttrIdAttrName);
            if (!att) {
                g_printerr ("start 'set' no attribute name set");
                break;
            }
            priv->attribute = laugh_attribute_from_string (att);
            if (to) {
                laugh_node_set_attribute (priv->target,
                        priv->attribute, to, &priv->changed_attribute);
            } /*TODO what to do if 'to' not specified*/
            break;
        }
        default:
            break;
    }
}

static void _laugh_animate_stop (LaughNode *node)
{
    LaughAnimate *self = (LaughAnimate *)node;
    LaughAnimatePrivate *priv = self->priv;

    if (!priv->target)
        return;

    g_object_weak_unref ((GObject *) priv->target,
            _laugh_animate_target_destroyed, priv);

    switch (node->id) {
        case LaughTagIdSet:
            if (priv->attribute)
                laugh_node_set_attribute (priv->target,
                        priv->attribute, NULL, &priv->changed_attribute);
            break;
        default:
            break;
    }

    priv->target = NULL;
}

static LaughRole *_laugh_animate_role (LaughNode *node, LaughRoleType type)
{
    LaughAnimate *self = (LaughAnimate *) node;

    switch (type) {
        case LaughRoleTypeTiming:
            return (LaughRole *) self->priv->timing_role;
        default:
            return NULL;
    }
}

static void laugh_animate_class_init (LaughAnimateClass *klass)
{
    GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
    LaughNodeClass *node_class = (LaughNodeClass *) klass;

    laugh_animate_parent_class = g_type_class_peek_parent (klass);

    gobject_class->finalize = laugh_animate_finalize;
    gobject_class->dispose = laugh_animate_dispose;
    node_class->init = _laugh_animate_init;
    node_class->set_attribute = _laugh_animate_set_attribute;
    node_class->start = _laugh_animate_start;
    node_class->stop = _laugh_animate_stop;
    node_class->role = _laugh_animate_role;

    g_type_class_add_private (gobject_class, sizeof (LaughAnimatePrivate));
}

static
void laugh_animate_instance_init (GTypeInstance *instance, gpointer g_class)
{
    LaughAnimate *self = (LaughAnimate *)instance;

    self->priv = LAUGH_ANIMATE_GET_PRIVATE (self);

    self->priv->timing_role = laugh_timing_role_new ((LaughNode *) self);
}

GType laugh_animate_get_type (void)
{
    static GType type = 0;
    if (type == 0) {
        static const GTypeInfo info = {
            sizeof (LaughAnimateClass),
            NULL,   /* base_init */
            NULL,   /* base_finalize */
            (GClassInitFunc) laugh_animate_class_init,   /* class_init */
            NULL,   /* class_finalize */
            NULL,   /* class_data */
            sizeof (LaughAnimate),
            0,      /* n_preallocs */
            laugh_animate_instance_init    /* instance_init */
        };
        type = g_type_register_static (LAUGH_TYPE_NODE,
                "LaughAnimateType",
                &info, 0);
    }
    return type;
}

LaughNode *laugh_animate_new (LaughDocument *doc, LaughNodeTagId id,
        GHashTable *attributes)
{
    LaughNode *n = (LaughNode *)g_object_new(LAUGH_TYPE_ANIMATE, NULL);

    laugh_node_base_construct (doc, n, id, attributes);

    return n;
}
