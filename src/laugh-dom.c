/*
 * Laugh.
 *
 * An glib SMIL library.
 *
 * Authored By Koos Vriezen  <koos.vriezen@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/**
 * SECTION:laugh-io
 * @short_description: DOM bases classes.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "laugh-dom.h"
#include "laugh-io.h"
#include "laugh-layout.h"
#include "laugh-timing.h"
#include "laugh-animate.h"
#include "laugh-media.h"
#include "laugh-linking.h"

#include <string.h>
#include <expat.h>
#include <glib/gprintf.h>

#define LAUGH_NODE_GET_PRIVATE(obj) \
(G_TYPE_INSTANCE_GET_PRIVATE ((obj), LAUGH_TYPE_NODE, LaughNodePrivate))
#define LAUGH_DOCUMENT_GET_PRIVATE(obj) \
(G_TYPE_INSTANCE_GET_PRIVATE ((obj), LAUGH_TYPE_DOCUMENT, LaughDocumentPrivate))

enum
{
    INITIALIZED,
    STARTED,
    STOPPED,

    LAST_SIGNAL
};

typedef struct _DocumentBuilder
{
    LaughNode *top_node;
    LaughNode *current;
} DocumentBuilder;

typedef struct _LaughIdMapping
{
    GHashTable *string_id;
    GHashTable *id_string;
    long string_unknown;
} LaughIdMapping;

static guint laugh_node_signals[LAST_SIGNAL] = { 0 };

static LaughIdMapping _laugh_tag_map;
static LaughIdMapping _laugh_attr_map;


struct _LaughNodePrivate
{
    GHashTable *attribute_undo;
};

static gpointer laugh_node_parent_class = ((void *)0);

static void laugh_node_finalize (GObject *object)
{
    G_OBJECT_CLASS (laugh_node_parent_class)->finalize (object);
}

static void _laugh_node_attr_free (gpointer key, gpointer value, gpointer data)
{
    (void) key; (void) data;

    g_free (value);
}

static void _laugh_node_undo_free (gpointer key, gpointer value, gpointer data)
{
    GSList *list;
    (void) key; (void) data;

    for (list = (GSList *) value; list; list = list->next)
        g_free (list->data);
    g_slist_free (list);
}

static void _laugh_node_children_clear (LaughNode *node)
{
    LaughNode *child;

    for (child = node->first_child; child; ) {
        LaughNode *tmp = child->next_sibling;
        g_object_unref (child);
        child = tmp;
    }
    node->first_child = node->last_child = NULL;
}

static void laugh_node_dispose (GObject *object)
{
    LaughNode *node = LAUGH_NODE(object);
    LaughNodePrivate *priv = node->priv;

    if (node->attributes) {
        g_hash_table_foreach (node->attributes, _laugh_node_attr_free, NULL);
        g_hash_table_destroy (node->attributes);
    }

    if (priv->attribute_undo) {
        g_hash_table_foreach (priv->attribute_undo, _laugh_node_undo_free,NULL);
        g_hash_table_destroy (priv->attribute_undo);
    }

    _laugh_node_children_clear (node);

    G_OBJECT_CLASS (laugh_node_parent_class)->dispose (object);
}

static void _laugh_node_set_attribute (LaughNode *node,
        LaughNodeAttributeId attr, const gchar *val, gpointer *undo)
{
    if (LaughAttrIdId == attr)
        node->element_id = laugh_document_id_from_string (node->document, val);

    /*This is ugly, during initialize g_hash_table_foreach call us */
    if (node->state > LaughStateInit) {
        LaughNodePrivate *priv = node->priv;
        gchar *cur = NULL;
        gchar *new_cur = NULL;

        if (node->attributes)
            cur = g_hash_table_lookup (node->attributes, (gpointer)(long) attr);
        else
            node->attributes = g_hash_table_new (NULL, NULL);

        if (cur == val)
            return;

        if (val) {
            new_cur = g_strdup (val);
            g_hash_table_replace (node->attributes,
                    (gpointer) (long) attr, new_cur);
        } else {
            g_hash_table_remove (node->attributes, (gpointer) (long) attr);
        }

        if (undo) {

            if (val) { /*set*/
                *(const gchar **)undo = new_cur;
                g_printf ("set undo to %s\n", new_cur);

                if (cur) {
                    GSList *ol = NULL;

                    if (priv->attribute_undo)
                        ol = (GSList *) g_hash_table_lookup (
                                priv->attribute_undo, (gpointer) (long) attr);
                    else
                        priv->attribute_undo = g_hash_table_new (NULL, NULL);

                    ol = g_slist_prepend (ol, cur);
                    g_hash_table_replace (priv->attribute_undo,
                            (gpointer) (long) attr, ol);
                }
            } else {   /*unset*/
                const gchar *undo_val = *(gchar **)undo;

                if (undo_val && priv->attribute_undo) {
                    GSList *ol = ol = (GSList *) g_hash_table_lookup (
                            priv->attribute_undo, (gpointer) (long) attr);

                    if (ol) {
                        if (undo_val == cur) {
                            new_cur = ol->data;
                            g_hash_table_replace (node->attributes,
                                    (gpointer) (long) attr, new_cur);
                            ol = ol->next;
                        } else {
                            ol = g_slist_remove (ol, undo_val);
                            g_free (undo_val);
                        }

                        if (!ol)
                            g_hash_table_remove (priv->attribute_undo,
                                    (gpointer) (long) attr);
                        else
                            g_hash_table_replace (priv->attribute_undo,
                                    (gpointer) (long) attr, ol);
                    }
                }

                if (cur)
                    g_free (cur);

                g_printf ("set undo to %s\n", new_cur ? new_cur : "-");
                *(gchar **)undo = new_cur;
            }
        } else if (cur) {
            g_free (cur);
        }
    }
}

static const gchar *_laugh_node_get_attribute (LaughNode *node,
        LaughNodeAttributeId att)
{
    if (node->attributes)
        return (gchar *) g_hash_table_lookup (
                node->attributes, (gpointer) (long) att);
    return NULL;
}

static void _laugh_node_init (LaughNode *node, LaughInitializer *initializer)
{
    LaughNode *child;

    if (node->attributes)
        g_hash_table_foreach (node->attributes, laugh_attributes_set, node);

    for (child = node->first_child; child; child = child->next_sibling)
        laugh_node_init (child, initializer);
}

static void _laugh_node_start (LaughNode *node)
{
    node->state = LaughStateBegun;
    g_signal_emit (node, laugh_node_signals[STARTED], 0);
}

static void _laugh_node_stop (LaughNode *node)
{
    node->state = LaughStateStopped;
    g_signal_emit (node, laugh_node_signals[STOPPED], 0);
}

static void _laugh_node_freeze (LaughNode *node)
{
    node->state = LaughStateFreezed;
}

static void _laugh_add_mapping (LaughIdMapping *m, long id, const gchar *s)
{
    g_hash_table_insert (m->string_id, (gpointer) s, (gpointer) id);
    g_hash_table_insert (m->id_string, (gpointer) id, (gpointer) s);
}

static void laugh_node_class_init (LaughNodeClass *klass)
{
    GObjectClass *gobject_class = G_OBJECT_CLASS (klass);

    laugh_node_parent_class = g_type_class_peek_parent (klass);

    gobject_class->finalize = laugh_node_finalize;
    gobject_class->dispose = laugh_node_dispose;
    klass->set_attribute = _laugh_node_set_attribute;
    klass->get_attribute = _laugh_node_get_attribute;
    klass->init = _laugh_node_init;
    klass->freeze = _laugh_node_freeze;

    g_type_class_add_private (gobject_class, sizeof (LaughNodePrivate));

    laugh_node_signals[INITIALIZED] =
        g_signal_new ("initialized",
                G_TYPE_FROM_CLASS (gobject_class),
                G_SIGNAL_RUN_LAST,
                G_STRUCT_OFFSET (LaughNodeClass, initialized),
                NULL, NULL,
                g_cclosure_marshal_VOID__VOID,
                G_TYPE_NONE, 0);

    laugh_node_signals[STARTED] =
        g_signal_new ("started",
                G_TYPE_FROM_CLASS (gobject_class),
                G_SIGNAL_RUN_LAST,
                G_STRUCT_OFFSET (LaughNodeClass, started),
                NULL, NULL,
                g_cclosure_marshal_VOID__VOID,
                G_TYPE_NONE, 0);

    laugh_node_signals[STOPPED] =
        g_signal_new ("stopped",
                G_TYPE_FROM_CLASS (gobject_class),
                G_SIGNAL_RUN_LAST,
                G_STRUCT_OFFSET (LaughNodeClass, stopped),
                NULL, NULL,
                g_cclosure_marshal_VOID__VOID,
                G_TYPE_NONE, 0);

    _laugh_tag_map.string_id = g_hash_table_new (g_str_hash, g_str_equal);
    _laugh_tag_map.id_string = g_hash_table_new (NULL, NULL);
    _laugh_tag_map.string_unknown = LaughTagIdLast;

    _laugh_attr_map.string_id = g_hash_table_new (g_str_hash, g_str_equal);
    _laugh_attr_map.id_string = g_hash_table_new (NULL, NULL);
    _laugh_attr_map.string_unknown = LaughAttrIdLast;

    _laugh_add_mapping (&_laugh_tag_map, LaughTagIdDocument, "document");
    _laugh_add_mapping (&_laugh_tag_map, LaughTagIdSmil, "smil");
    _laugh_add_mapping (&_laugh_tag_map, LaughTagIdHead, "head");
    _laugh_add_mapping (&_laugh_tag_map, LaughTagIdLayout, "layout");
    _laugh_add_mapping (&_laugh_tag_map, LaughTagIdRootLayout, "root-layout");
    _laugh_add_mapping (&_laugh_tag_map, LaughTagIdRegion, "region");
    _laugh_add_mapping (&_laugh_tag_map, LaughTagIdBody, "body");
    _laugh_add_mapping (&_laugh_tag_map, LaughTagIdSeq, "seq");
    _laugh_add_mapping (&_laugh_tag_map, LaughTagIdPar, "par");
    _laugh_add_mapping (&_laugh_tag_map, LaughTagIdExcl, "excl");
    _laugh_add_mapping (&_laugh_tag_map, LaughTagIdSwitch, "switch");
    _laugh_add_mapping (&_laugh_tag_map, LaughTagIdAudio, "audio");
    _laugh_add_mapping (&_laugh_tag_map, LaughTagIdBrush, "brush");
    _laugh_add_mapping (&_laugh_tag_map, LaughTagIdImg, "img");
    _laugh_add_mapping (&_laugh_tag_map, LaughTagIdRef, "ref");
    _laugh_add_mapping (&_laugh_tag_map, LaughTagIdText, "text");
    _laugh_add_mapping (&_laugh_tag_map, LaughTagIdVideo, "video");
    _laugh_add_mapping (&_laugh_tag_map, LaughTagIdSet, "set");
    _laugh_add_mapping (&_laugh_tag_map, LaughTagIdAnimate, "animate");
    _laugh_add_mapping (&_laugh_tag_map, LaughTagIdAnimateMotion, "animateMotion");
    _laugh_add_mapping (&_laugh_tag_map, LaughTagIdA, "a");
    _laugh_add_mapping (&_laugh_tag_map, LaughTagIdAnchor, "anchor");
    _laugh_add_mapping (&_laugh_tag_map, LaughTagIdArea, "area");

    _laugh_add_mapping (&_laugh_attr_map, LaughAttrIdId, "id");
    _laugh_add_mapping (&_laugh_attr_map, LaughAttrIdName, "name");
    _laugh_add_mapping (&_laugh_attr_map, LaughAttrIdLeft, "left");
    _laugh_add_mapping (&_laugh_attr_map, LaughAttrIdTop, "top");
    _laugh_add_mapping (&_laugh_attr_map, LaughAttrIdWidth, "width");
    _laugh_add_mapping (&_laugh_attr_map, LaughAttrIdHeight, "height");
    _laugh_add_mapping (&_laugh_attr_map, LaughAttrIdRight, "right");
    _laugh_add_mapping (&_laugh_attr_map, LaughAttrIdBottom, "bottom");
    _laugh_add_mapping (&_laugh_attr_map, LaughAttrIdBgColor, "background-color");
    _laugh_add_mapping (&_laugh_attr_map, LaughAttrIdBgColor1, "backgroundColor");
    _laugh_add_mapping (&_laugh_attr_map, LaughAttrIdBgImage, "backgroundImage");
    _laugh_add_mapping (&_laugh_attr_map, LaughAttrIdShowBg, "showBackground");
    _laugh_add_mapping (&_laugh_attr_map, LaughAttrIdZIndex, "z-index");
    _laugh_add_mapping (&_laugh_attr_map, LaughAttrIdBegin, "begin");
    _laugh_add_mapping (&_laugh_attr_map, LaughAttrIdDur, "dur");
    _laugh_add_mapping (&_laugh_attr_map, LaughAttrIdEnd, "end");
    _laugh_add_mapping (&_laugh_attr_map, LaughAttrIdEndSync, "endSync");
    _laugh_add_mapping (&_laugh_attr_map, LaughAttrIdRepeat, "repeat");
    _laugh_add_mapping (&_laugh_attr_map, LaughAttrIdSrc, "src");
    _laugh_add_mapping (&_laugh_attr_map, LaughAttrIdRegion, "region");
    _laugh_add_mapping (&_laugh_attr_map, LaughAttrIdType, "type");
    _laugh_add_mapping (&_laugh_attr_map, LaughAttrIdSubType, "subType");
    _laugh_add_mapping (&_laugh_attr_map, LaughAttrIdFill, "fil");
    _laugh_add_mapping (&_laugh_attr_map, LaughAttrIdFit, "fit");
    _laugh_add_mapping (&_laugh_attr_map, LaughAttrIdPeers, "peers");
    _laugh_add_mapping (&_laugh_attr_map, LaughAttrIdHigher, "higher");
    _laugh_add_mapping (&_laugh_attr_map, LaughAttrIdLower, "lower");
    _laugh_add_mapping (&_laugh_attr_map, LaughAttrIdPauseDisplay, "pauseDisplay");
    _laugh_add_mapping (&_laugh_attr_map, LaughAttrIdHRef, "href");
    _laugh_add_mapping (&_laugh_attr_map, LaughAttrIdCoord, "coords");
    _laugh_add_mapping (&_laugh_attr_map, LaughAttrIdTransIn, "trans-in");
    _laugh_add_mapping (&_laugh_attr_map, LaughAttrIdTransOut, "trans-out");
    _laugh_add_mapping (&_laugh_attr_map, LaughAttrIdSystemBitrate, "system-bitrate");
    _laugh_add_mapping (&_laugh_attr_map, LaughAttrIdSensitivity, "sensitivity");
    _laugh_add_mapping (&_laugh_attr_map, LaughAttrIdFontColor, "fontColor");
    _laugh_add_mapping (&_laugh_attr_map, LaughAttrIdFontSize, "fontSize");
    _laugh_add_mapping (&_laugh_attr_map, LaughAttrIdHAlign, "hAlign");
    _laugh_add_mapping (&_laugh_attr_map, LaughAttrIdVAlign, "vAlign");
    _laugh_add_mapping (&_laugh_attr_map, LaughAttrIdCharset, "charset");
    _laugh_add_mapping (&_laugh_attr_map, LaughAttrIdBgOpacity, "backgroundOpacity");
    _laugh_add_mapping (&_laugh_attr_map, LaughAttrIdTargetElement, "targetElement");
    _laugh_add_mapping (&_laugh_attr_map, LaughAttrIdTarget, "target");
    _laugh_add_mapping (&_laugh_attr_map, LaughAttrIdAttr, "attribute");
    _laugh_add_mapping (&_laugh_attr_map, LaughAttrIdAttrName, "attributeName");
    _laugh_add_mapping (&_laugh_attr_map, LaughAttrIdTo, "to");
    _laugh_add_mapping (&_laugh_attr_map, LaughAttrIdChangeBy, "changed-by");
    _laugh_add_mapping (&_laugh_attr_map, LaughAttrIdFrom, "from");
    _laugh_add_mapping (&_laugh_attr_map, LaughAttrIdValues, "values");
    _laugh_add_mapping (&_laugh_attr_map, LaughAttrIdCalcMode, "calcMode");
    _laugh_add_mapping (&_laugh_attr_map, LaughAttrIdKeyTimes, "keyTimes");
    _laugh_add_mapping (&_laugh_attr_map, LaughAttrIdKeySplines, "keySplines");
}

static void laugh_node_instance_init (GTypeInstance *instance, gpointer g_class)
{
    LaughNode *self = (LaughNode *)instance;

    self->priv = LAUGH_NODE_GET_PRIVATE (self);

    self->state = LaughStateInit;
    self->parent_node = NULL;
    self->first_child = NULL;
    self->previous_sibling = NULL;
    self->next_sibling = NULL;
    self->attributes = NULL;
}

GType laugh_node_get_type (void)
{
    static GType type = 0;
    if (type == 0) {
        static const GTypeInfo info = {
            sizeof (LaughNodeClass),
            NULL,   /* base_init */
            NULL,   /* base_finalize */
            (GClassInitFunc) laugh_node_class_init,   /* class_init */
            NULL,   /* class_finalize */
            NULL,   /* class_data */
            sizeof (LaughNode),
            0,      /* n_preallocs */
            laugh_node_instance_init    /* instance_init */
        };
        type = g_type_register_static (G_TYPE_OBJECT,
                "LaughNodeType",
                &info, 0);
    }
    return type;
}

struct _LaughDocumentPrivate
{
    gchar *uri;
    LaughIO *io;
    GHashTable *element_id_table;
    guint element_id_last;
    ClutterContainer *parent_actor;
    guint width;
    guint height;
};

static gpointer laugh_document_parent_class = ((void *)0);

static void laugh_document_finalize (GObject *object)
{
    G_OBJECT_CLASS (laugh_document_parent_class)->finalize (object);
}

static void laugh_document_dispose (GObject *object)
{
    LaughDocument *document = LAUGH_DOCUMENT(object);
    LaughRoleTiming *role = document->timing;

    g_free (document->priv->uri);
    if (document->priv->element_id_table)
        g_hash_table_destroy (document->priv->element_id_table);

    G_OBJECT_CLASS (laugh_document_parent_class)->dispose (object);

    laugh_timing_role_delete (role);
}

static
void _lauch_document_mime_type (LaughIO *io, const gchar *mime, gpointer d)
{
    g_printf ("lauch_document_mime_type: %s\n", mime);
    if (strcmp (mime, "application/smil")) {
        LaughNode *node = LAUGH_NODE (d);

        g_printerr ("not a smil document\n");
        laugh_io_cancel (io);

        g_object_unref (G_OBJECT (io));

        laugh_node_base_emit_initialized (node);
    }
}

static void
_lauch_document_completed (LaughIO *io, gsize sz, gpointer data, gpointer d)
{
    LaughInitializer *initializer = (LaughInitializer *)d;
    LaughNode *node = initializer->node;

    g_printf ("_lauch_document_completed: %u\n", sz);
    if (data) {
        laugh_node_set_inner_xml (node, (const gchar *) data);

        g_free (data);

        if (node->first_child)
            laugh_node_init (node->first_child, initializer);
    }
    g_object_unref (G_OBJECT (io));

    if (!initializer->init_pending)
        laugh_node_base_emit_initialized (node);
}

static void _laugh_document_init (LaughNode *doc, LaughInitializer *initializer)
{
    LaughDocument *document = (LaughDocument *) doc;
    LaughDocumentPrivate *priv = document->priv;

    initializer->node = doc;
    initializer->parent_segment = document->timing;
    priv->io = laugh_io_new (priv->uri, NULL);
    priv->element_id_table = g_hash_table_new_full (g_str_hash, g_str_equal,
            (GDestroyNotify) g_free, NULL);
    priv->parent_actor = CLUTTER_ACTOR (initializer->parent_region);
    g_printf( "_laugh_document_init %d %d %d %d\n",
            priv->width, priv->height, initializer->parent_width, initializer->parent_height);
    priv->width = initializer->parent_width;
    priv->height = initializer->parent_height;

    g_signal_connect (G_OBJECT (priv->io), "mime-type",
            (GCallback) _lauch_document_mime_type, (gpointer) doc);
    g_signal_connect (G_OBJECT (priv->io), "completed",
            (GCallback) _lauch_document_completed, (gpointer) initializer);

    g_free (priv->uri);
    priv->uri = g_strdup (laugh_io_get_uri (priv->io));

    laugh_io_open (priv->io);
}

static void _laugh_document_start (LaughNode *node)
{
    LaughDocument *doc = (LaughDocument *) node;

    if (!doc->timing->active)
        laugh_timing_setting_start (doc->timing);
}

static void _laugh_document_stop (LaughNode *node)
{
    LaughDocument *doc = (LaughDocument *) node;

    if (doc->timing->active)
        laugh_timing_setting_stop (doc->timing);
}

static void laugh_document_class_init (LaughDocumentClass *klass)
{
    GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
    LaughNodeClass *node_class = (LaughNodeClass *) klass;

    laugh_document_parent_class = g_type_class_peek_parent (klass);

    gobject_class->finalize = laugh_document_finalize;
    gobject_class->dispose = laugh_document_dispose;
    node_class->init = _laugh_document_init;
    node_class->start = _laugh_document_start;
    node_class->stop = _laugh_document_stop;

    g_type_class_add_private (gobject_class, sizeof (LaughDocumentPrivate));
}

static
void laugh_document_instance_init (GTypeInstance *instance, gpointer g_class)
{
    LaughDocument *self = (LaughDocument *)instance;

    self->priv = LAUGH_DOCUMENT_GET_PRIVATE (self);
    self->timing = laugh_timing_role_new ((LaughNode *) self);
}

GType laugh_document_get_type (void)
{
    static GType type = 0;
    if (type == 0) {
        static const GTypeInfo info = {
            sizeof (LaughDocumentClass),
            NULL,   /* base_init */
            NULL,   /* base_finalize */
            (GClassInitFunc) laugh_document_class_init,   /* class_init */
            NULL,   /* class_finalize */
            NULL,   /* class_data */
            sizeof (LaughDocument),
            0,      /* n_preallocs */
            laugh_document_instance_init    /* instance_init */
        };
        type = g_type_register_static (LAUGH_TYPE_NODE,
                "LaughDocumentType",
                &info, 0);
    }
    return type;
}

static long _laugh_mapping_from_string (LaughIdMapping *map, const gchar *tag)
{
    gpointer id = g_hash_table_lookup (map->string_id, (gpointer) tag);
    if (!id) {
        id = (gpointer) map->string_unknown++;
        _laugh_add_mapping (map, (long) id, g_strdup (tag));
    }
    return (long) id;
}

LaughNodeTagId laugh_tag_from_string (const gchar *tag)
{
    return (LaughNodeTagId) _laugh_mapping_from_string (&_laugh_tag_map, tag);
}

const gchar *laugh_tag_from_id (LaughNodeTagId id)
{
    return (const gchar *) g_hash_table_lookup (_laugh_tag_map.id_string,
            (gpointer) id);
}

LaughNodeAttributeId laugh_attribute_from_string (const gchar *attr)
{
    return (LaughNodeAttributeId)_laugh_mapping_from_string (
            &_laugh_attr_map, attr);
}

const gchar *laugh_attribute_from_id (LaughNodeAttributeId id)
{
    return (const gchar *) g_hash_table_lookup (_laugh_attr_map.id_string,
            (gpointer) id);
}

static void
_laugh_dom_initializer_initialized (LaughNode *node, LaughInitializer *init)
{
    init->init_pending = g_slist_remove (init->init_pending, node);
    if (!init->init_pending)
        laugh_node_base_emit_initialized (init->node);
}

LaughInitializer *laugh_dom_initializer_new (ClutterContainer *parent,
        guint width, guint height)
{
    LaughInitializer *initializer = g_new0 (LaughInitializer, 1);

    initializer->parent_region = parent;
    initializer->parent_width = width;
    initializer->parent_height = height;
    initializer->initialized = _laugh_dom_initializer_initialized;

    return initializer;
}

LaughNode *laugh_node_new (LaughDocument *document, LaughNodeTagId id,
                           GHashTable *attrs)
{
    LaughNode *node = LAUGH_NODE (g_object_new (LAUGH_TYPE_NODE, NULL));

    laugh_node_base_construct (document, node, id, attrs);

    return node;
}

LaughDocument *laugh_document_new (const gchar *uri)
{
    LaughDocument *d = LAUGH_DOCUMENT(g_object_new (LAUGH_TYPE_DOCUMENT, NULL));
    LaughNode *node = (LaughNode *)d;

    laugh_node_base_construct (d, node, LaughTagIdDocument, NULL);
    d->priv->uri = g_strdup (uri);

    return d;
}

void laugh_node_base_construct (LaughDocument *document, LaughNode *node,
        LaughNodeTagId id, GHashTable *attributes)
{
    node->id = id;
    node->document = document;
    node->attributes = attributes;
}

void laugh_attributes_set (gpointer key, gpointer val, gpointer data)
{
    laugh_node_set_attribute ((LaughNode *)data,
            (LaughNodeAttributeId) (long) key, (const gchar *) val, NULL);
}

void laugh_node_base_emit_initialized (LaughNode *node)
{
    g_signal_emit (node, laugh_node_signals[INITIALIZED], 0);
}

const gchar *laugh_node_get_attribute (LaughNode *node,
        LaughNodeAttributeId attribute)
{
    LaughNodeClass *klass = LAUGH_NODE_GET_CLASS(node);

    return klass->get_attribute (node, attribute);
}

void laugh_node_set_attribute (LaughNode *node,
        LaughNodeAttributeId attr, const gchar *value, gpointer *undo)
{
    LaughNodeClass *klass = LAUGH_NODE_GET_CLASS(node);

    klass->set_attribute (node, attr, value, undo);
}

static
void _laugh_xml_start_tag (void *data, const char *tag, const char **attr)
{
    DocumentBuilder *builder = (DocumentBuilder *) data;
    LaughNode *node;
    GHashTable *attrs = NULL;
    LaughNodeTagId id = laugh_tag_from_string (tag);

    if (attr && attr[0]) {
        int i;
        attrs = g_hash_table_new (NULL, NULL);
        for (i = 0; attr[i]; i += 2)
            g_hash_table_insert (attrs,
                    (gpointer) (long) laugh_attribute_from_string (attr[i]),
                    g_strdup (attr[i+1]));
    }
    switch (id) {
        case LaughTagIdLayout:
        case LaughTagIdRootLayout:
        case LaughTagIdRegion:
            node = laugh_layout_new (builder->top_node->document, id, attrs);
            break;
        case LaughTagIdBody:
        case LaughTagIdSeq:
        case LaughTagIdPar:
        case LaughTagIdExcl:
            node = laugh_timing_container_new (builder->top_node->document,
                    id, attrs);
            break;
        case LaughTagIdSet:
        case LaughTagIdAnimate:
        case LaughTagIdAnimateMotion:
            node = laugh_animate_new (builder->top_node->document, id, attrs);
            break;
        case LaughTagIdAudio:
        case LaughTagIdBrush:
        case LaughTagIdImg:
        case LaughTagIdRef:
        case LaughTagIdText:
        case LaughTagIdVideo:
            node = laugh_media_new (builder->top_node->document, id, attrs);
            break;
        case LaughTagIdA:
        case LaughTagIdAnchor:
        case LaughTagIdArea:
            node = laugh_linking_new (builder->top_node->document, id, attrs);
            break;
        default:
            node = laugh_node_new (builder->top_node->document, id, attrs);
    }
    laugh_node_append_child (builder->current, node);

    builder->current = node;
}

static void _laugh_xml_end_tag (void *data, const char *tag)
{
    DocumentBuilder *builder = (DocumentBuilder *) data;

    if (laugh_tag_from_string (tag) != builder->current->id)
        g_printerr ("unmatched tag %s, expects %s\n",
                tag, laugh_tag_from_id (builder->current->id));
    else if (builder->current != builder->top_node)
        builder->current = builder->current->parent_node;
    else
        g_printerr ("spurious end tag %s", tag);
}

void laugh_node_set_inner_xml (LaughNode *node, const gchar *xml)
{
    DocumentBuilder builder;
    XML_Parser parser = XML_ParserCreate (0L);

    _laugh_node_children_clear (node);

    builder.top_node = node;
    builder.current = node;

    XML_SetUserData (parser, &builder);
    XML_SetElementHandler (parser, _laugh_xml_start_tag, _laugh_xml_end_tag);

    if (XML_Parse (parser, xml, strlen (xml), TRUE) == XML_STATUS_ERROR) {
        /*TODO: clear all*/
    }

    XML_ParserFree(parser);
}

static void _laugh_node_get_outer_xml (LaughNode *node, GString *buffer, int sp)
{
    gchar *indent = "";

    if (sp > 0) {
        indent = (gchar *) g_malloc (sp + 1);
        memset (indent, ' ', sp);
        indent[sp] = 0;
        g_string_append (buffer, indent);
    }

    g_string_append_c (buffer, '<');
    g_string_append (buffer, laugh_tag_from_id (node->id));
    if (node->attributes) {
        GHashTableIter iter;
        gpointer key, value;
        g_hash_table_iter_init (&iter, node->attributes);
        while (g_hash_table_iter_next (&iter, &key, &value)) {
            g_string_append_c (buffer, ' ');
            g_string_append (buffer, laugh_attribute_from_id ((long) key));
            g_string_append (buffer, "=\"");
            g_string_append (buffer, (const gchar *)value);/*TODO escape*/
            g_string_append_c (buffer, '"');
        }
    }
    if (node->first_child) {
        LaughNode *child;
        g_string_append (buffer, ">\n");
        for (child = node->first_child; child; child = child->next_sibling)
            _laugh_node_get_outer_xml (child, buffer, sp+2);
        g_string_append (buffer, indent);
        g_string_append (buffer, "</");
        g_string_append (buffer, laugh_tag_from_id (node->id));
        g_string_append (buffer, ">\n");
    } else {
        g_string_append (buffer, "/>\n");
    }

    if (*indent)
        g_free (indent);
}

gchar *laugh_node_get_inner_xml (LaughNode *node)
{
    GString *buffer = g_string_new ("");
    LaughNode *child;
    gchar *xml;

    for (child = node->first_child; child; child = child->next_sibling)
        _laugh_node_get_outer_xml (child, buffer, 0);

    xml = g_string_free (buffer, FALSE);

    return xml;
}

void laugh_node_append_child (LaughNode *node, LaughNode *child)
{
    LaughNode *n = node->last_child;

    if (n) {
        n->next_sibling = child;
        child->previous_sibling = n;
        child->next_sibling = NULL;
    } else {
        node->first_child = child;
        child->previous_sibling = NULL;
        child->next_sibling = NULL;
    }
    node->last_child = child;
    child->parent_node = node;
}

void laugh_node_init (LaughNode *node, LaughInitializer *initializer)
{
    LaughNodeClass *klass = LAUGH_NODE_GET_CLASS(node);

    klass->init (node, initializer);
}

void laugh_node_start (LaughNode *node)
{
    LaughNodeClass *klass = LAUGH_NODE_GET_CLASS(node);

    if (klass->start)
        klass->start (node);
    _laugh_node_start (node);
}

void laugh_node_stop (LaughNode *node)
{
    LaughNodeClass *klass = LAUGH_NODE_GET_CLASS(node);

    if (klass->stop)
        klass->stop (node);
    _laugh_node_stop (node);
}

void laugh_node_freeze (LaughNode *node)
{
    LaughNodeClass *klass = LAUGH_NODE_GET_CLASS(node);

    if (klass->freeze)
        klass->freeze (node);
}

guint laugh_document_id_from_string (LaughDocument *doc, const gchar *str)
{
    LaughDocumentPrivate *priv = doc->priv;
    guint id = (long) g_hash_table_lookup (priv->element_id_table, str);

    if (!id) {
        id = ++priv->element_id_last;
        g_hash_table_insert (priv->element_id_table,
                g_strdup (str), (gpointer) (long) id);
    }

    return id;
}

LaughNode *_laugh_node_get_element_by_id (LaughNode *node, const guint id)
{
    LaughNode *child;

    if (id == node->element_id)
        return node;

    for (child = node->first_child; child; child = child->next_sibling) {
        LaughNode *n = _laugh_node_get_element_by_id (child, id);
        if (n)
            return n;
    }

    return NULL;
}

LaughNode *laugh_document_get_element_by_id (LaughDocument *doc, guint id)
{
    return _laugh_node_get_element_by_id ((LaughNode *) doc, id);
}

const gchar *laugh_document_get_uri (LaughDocument *doc)
{
    return doc->priv->uri;
}

LaughRole *laugh_node_role_get (LaughNode *node, LaughRoleType type)
{
    LaughNodeClass *klass = LAUGH_NODE_GET_CLASS(node);

    if (klass->role)
        return klass->role (node, type);

    return NULL;
}

void laugh_document_set_uri (LaughDocument *doc, const gchar *uri)
{
    LaughNode *node = LAUGH_NODE (doc);
    LaughDocumentPrivate *priv = doc->priv;

    _laugh_node_children_clear (node);

    if (priv->uri) {
        gchar *new_uri = laugh_io_uri_resolve (uri, priv->uri);
        g_free (priv->uri);
        priv->uri = new_uri;
    } else {
        priv->uri = g_strdup (uri);
    }

    laugh_timing_setting_stop (doc->timing);
    node->state = LaughStateInit;
}

static void _laugh_document_initialized (LaughNode *node, LaughInitializer *initializer)
{
    g_signal_handler_disconnect (G_OBJECT (node), initializer->init_signal);

    g_free (initializer);

    laugh_node_start (node);
}

gboolean
laugh_document_open (LaughDocument *doc, ClutterContainer *parent, gint w, gint h)
{
    /*TODO: create an actor instead of passing a parent and wxh*/
    LaughInitializer *initializer;
    LaughDocumentPrivate *priv = doc->priv;

    if (!priv->uri) {
        g_printerr ("laugh_document_open: no uri set\n");
        return FALSE;
    }

    if (!parent)
        parent = priv->parent_actor;
    if (w < 0)
        w = priv->width;
    if (h < 0)
        h = priv->height;

    initializer = laugh_dom_initializer_new (parent, w, h);

    initializer->init_signal = g_signal_connect (G_OBJECT (doc), "initialized",
            (GCallback) _laugh_document_initialized, initializer);

    laugh_node_init (LAUGH_NODE (doc), initializer);

    clutter_actor_show_all (CLUTTER_ACTOR (parent));

    return TRUE;
}

#ifdef LAUGH_DOM_TEST
/* gcc laugh-dom.c laugh-io.c laugh-layout.c -o domtest `pkg-config --libs --cflags gio-2.0 glib-2.0 gthread-2.0 clutter-gtk-0.6` -lexpat -DLAUGH_DOM_TEST */
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

static GMainLoop *mainloop;

void initialized_callback (LaughNode *node, gpointer d)
{
    gchar *xml = laugh_node_get_inner_xml (node);
    g_printf ("cb initialized\n%s\n", xml);

    g_free (xml);

    g_main_loop_quit (mainloop);
}

int main (int argc, char **argv)
{
    LaughDocument *doc;
    LaughInitializer *initializer;

    if (argc < 2) {
        g_printerr ("usage %s uri\n", argv[0]);
        return 1;
    }

    g_thread_init (NULL);
    g_type_init ();

    doc = laugh_document_new (argv[1]);
    initializer = laugh_dom_initializer_new (NULL, 640, 480);

    g_signal_connect (G_OBJECT (doc), "initialized",
            (GCallback) initialized_callback, (gpointer) 0);

    laugh_node_init (LAUGH_NODE (doc), initializer);

    mainloop = g_main_loop_new (NULL, TRUE);
    g_main_loop_run (mainloop);

    g_object_unref (G_OBJECT (doc));
    g_free (initializer);

    return 0;
}

#endif
