/*
 * Laugh.
 *
 * An glib SMIL library.
 *
 * Authored By Koos Vriezen  <koos.vriezen@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <string.h>
#include <glib/gmessages.h>
#include <glib/gprintf.h>
#include <clutter/clutter-main.h>

#include "laugh-timing.h"

#define LAUGH_TIMING_CONTAINER_GET_PRIVATE(obj) \
(G_TYPE_INSTANCE_GET_PRIVATE ((obj), LAUGH_TYPE_TIMING_CONTAINER, LaughTimingContainerPrivate))


static gpointer laugh_timing_container_parent_class = ((void *)0);

static void laugh_timing_container_finalize (GObject *object)
{
    G_OBJECT_CLASS (laugh_timing_container_parent_class)->finalize (object);
}

static void laugh_timing_container_dispose (GObject *object)
{
    LaughTimingContainer *self = LAUGH_TIMING_CONTAINER(object);
    LaughRoleTiming *segment = self->timing_role;

    G_OBJECT_CLASS (laugh_timing_container_parent_class)->dispose (object);

    laugh_timing_role_delete (segment);
}

static void
_laugh_timing_container_init (LaughNode *node, LaughInitializer *initializer)
{
    LaughTimingContainer *self = (LaughTimingContainer *) node;
    LaughNode *child;
    LaughRoleTiming *parent_segment = initializer->parent_segment;

    if (node->attributes)
        g_hash_table_foreach (node->attributes, laugh_attributes_set, node);

    laugh_timing_role_child_add (initializer->parent_segment, self->timing_role);

    initializer->parent_segment = self->timing_role;

    for (child = node->first_child; child; child = child->next_sibling)
        laugh_node_init (child, initializer);

    initializer->parent_segment = parent_segment;
}

static void _laugh_timing_container_set_attribute (LaughNode *node,
        LaughNodeAttributeId att, const gchar *value, gpointer *undo)
{
    const gchar *val = value;
    LaughTimingContainer *self = (LaughTimingContainer *)node;

    LAUGH_TYPE_NODE_CLASS(laugh_timing_container_parent_class)->
        set_attribute(node, att, val, undo);
    /*g_printf ("_laugh_timing_container_set_attribute %s=%s\n", laugh_attribute_from_id (att), val);*/

    if (!value && undo)
        val = *(const gchar **)undo;

    laugh_timing_setting_set_attribute (self->timing_role, att, val);
}

static LaughRole *_laugh_timing_container_role (LaughNode *node, LaughRoleType type)
{
    LaughTimingContainer *self = (LaughTimingContainer *) node;

    switch (type) {
        case LaughRoleTypeTiming:
            return (LaughRole *) self->timing_role;
        default:
            return NULL;
    }
}

static void laugh_timing_container_class_init (LaughTimingContainerClass *klass)
{
    GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
    LaughNodeClass *node_class = (LaughNodeClass *) klass;

    laugh_timing_container_parent_class = g_type_class_peek_parent (klass);

    gobject_class->finalize = laugh_timing_container_finalize;
    gobject_class->dispose = laugh_timing_container_dispose;
    node_class->init = _laugh_timing_container_init;
    node_class->set_attribute = _laugh_timing_container_set_attribute;
    node_class->role = _laugh_timing_container_role;

    /*g_type_class_add_private (gobject_class, sizeof (LaughTimingContainerPrivate));*/
}

static
void laugh_timing_container_instance_init (GTypeInstance *instance, gpointer g_class)
{
    LaughTimingContainer *self = (LaughTimingContainer *)instance;

    /*self->priv = LAUGH_TIMING_CONTAINER_GET_PRIVATE (self);*/

    self->timing_role = laugh_timing_role_new ((LaughNode *) self);
}

GType laugh_timing_container_get_type (void)
{
    static GType type = 0;
    if (type == 0) {
        static const GTypeInfo info = {
            sizeof (LaughTimingContainerClass),
            NULL,   /* base_init */
            NULL,   /* base_finalize */
            (GClassInitFunc) laugh_timing_container_class_init,   /* class_init */
            NULL,   /* class_finalize */
            NULL,   /* class_data */
            sizeof (LaughTimingContainer),
            0,      /* n_preallocs */
            laugh_timing_container_instance_init    /* instance_init */
        };
        type = g_type_register_static (LAUGH_TYPE_NODE,
                "LaughTimingContainerType",
                &info, 0);
    }
    return type;
}

static int _timing_parse_time (const gchar *tstr, int *value) {
    int sign = 1;
    int fp_seen = 0;
    const gchar *nstart = NULL;
    const gchar *p = tstr;
    gchar *num;

    if (!tstr)
        return 0;

    for ( ; *p; p++) {
        if (*p == '+') {
            if (nstart)
                break;
            else
                sign = 1;
        } else if (*p == '-') {
            if (nstart)
                break;
            else
                sign = -1;
        } else if (*p >= '0' && *p <= '9') {
            if (!nstart)
                nstart = p;
        } else if (*p == '.') {
            if (fp_seen)
                break;
            fp_seen = 1;
            if (!nstart)
                nstart = p;
        } else if (*p == ' ') {
            if (nstart)
                break;
        } else
            break;
    }
    if (!nstart)
        return 0;

    num = g_strndup (nstart, p - tstr);
    *value = sign * (int) (1000 * g_strtod (nstart, NULL));

    for ( ; *p; p++ ) {
        if (*p == 'm') {
            *value *= 60;
            break;
        } else if (*p == 'h') {
            *value *= 3600;
            break;
        } else if (*p != ' ')
            break;
    }

    g_free (num);

    return 1;
}

static int _get_timing (const gchar *tstr, LaughTiming *timing) {
    const gchar *orig = tstr;
    gchar *lower;
    const gchar *p = tstr;
    gchar *idref = NULL;

    for ( ; *p; ++p, ++orig) {
        if (*p != ' ')
            break;
    }

    timing->type = LaughTimingUnknown;
    if (!p || !*p)
        return 0;

    lower = g_ascii_strdown (p, -1);
    p = lower;

    if (_timing_parse_time (lower, &timing->offset)) {
        timing->type = LaughTimingTime;
    } else if (!strncmp (lower, "id(", 3)) {
        p = (const gchar *) strchr (lower + 3, ')');
        if (p) {
            idref = g_strndup (orig + 3, p - lower - 3);
            p++;
        }
        if (*p) {
            const char *q = strchr (p, '(');
            if (q)
                p = q;
        }
    } else if (!strncmp (lower, "indefinite", 10)) {
        timing->type = LaughTimingIndefinite;
    } else if (!strncmp (lower, "media", 5)) {
        timing->type = LaughTimingMedia;
    }
    if (LaughTimingUnknown == timing->type) {
        const gchar *idref_start = p;
        if (!idref) {
            int last_esc = 0;
            for ( ; *p; p++) {
                if (*p == '\\') {
                    last_esc = last_esc ? 0 : 1;
                } else if (*p == '.' && !last_esc) {
                    break;
                }
            }
            if (!*p)
                idref = g_strdup (orig + (idref_start - lower));
            else
                idref = g_strndup (orig + (idref_start - lower), p - idref_start);
        }
        ++p;
        if (idref) {
            timing->element_id = idref;
            if (_timing_parse_time (p, &timing->offset)) {
                timing->type = LaughTimingStartSync;
            } else if (*p && !strncmp (p, "end", 3)) {
                timing->type = LaughTimingEndSync;
                _timing_parse_time (p+3, &timing->offset);
            } else if (*p && !strncmp (p, "begin", 5)) {
                timing->type = LaughTimingStartSync;
                _timing_parse_time (p+5, &timing->offset);
            } else if (*p && !strncmp (p, "activateevent", 13)) {
                timing->type = LaughTimingActivated;
                _timing_parse_time (p+13, &timing->offset);
            } else if (*p && !strncmp (p, "inboundsevent", 13)) {
                timing->type = LaughTimingInbounds;
                _timing_parse_time (p+13, &timing->offset);
            } else if (*p && !strncmp (p, "outofboundsevent", 16)) {
                timing->type = LaughTimingOutbounds;
                _timing_parse_time (p+16, &timing->offset);
            } else {
                g_printerr ("get_timings no match %s", lower);
            }
        }
    }
    g_free (lower);
    return 1;
}

LaughTiming *laugh_timing_new ()
{
    return g_new0 (LaughTiming, 1);
}

static void _laugh_timing_element_destroyed (LaughTiming *timing, GObject *obj)
{
    if (timing->handler_id) {
        timing->handler_id = 0;
        timing->element = 0;
    } else {
        g_printerr ("laugh_timing spurious destroy signal\n");
    }
}

static gboolean _laugh_timing_timeout (gpointer data);
static void _laugh_timing_setting_started (LaughRoleTiming *segment);
static void _laugh_timing_setting_stopped (LaughRoleTiming *segment);

static void _laugh_timing_time_connect (LaughTiming *timing, int t, gpointer data)
{
    timing->timeout_id = g_timeout_add (t > 0 ? t : 0, _laugh_timing_timeout, data);
}

static void _laugh_timing_source_connect (LaughTiming *timing, LaughNode *source,
        const gchar *signal, GCallback func, gpointer data)
{
    timing->element = source;
    g_object_weak_ref ((GObject *) source, _laugh_timing_element_destroyed, timing);
    timing->handler_id = g_signal_connect (G_OBJECT (source), signal, func, data);
}

static void _laugh_timing_source_disconnect (LaughTiming *timing)
{
    if (timing->handler_id) {
        switch (timing->type) {
            case LaughTimingTime:
            case LaughTimingMedia:
                break;
            default:
                if (timing->element) {
                    g_signal_handler_disconnect (timing->element, timing->handler_id);
                    g_object_weak_unref ((GObject *) timing->element,
                            _laugh_timing_element_destroyed, timing);
                }
                timing->element = NULL;
                timing->handler_id = 0;
                break;
        }
    }
    if (timing->timeout_id) {
        g_source_remove (timing->timeout_id);
        timing->timeout_id = 0;
    }
}

void laugh_timing_delete (LaughTiming *timing)
{
    _laugh_timing_source_disconnect (timing);

    if (timing->element_id)
        g_free (timing->element_id);
    g_free (timing);
}

LaughRoleTiming *laugh_timing_role_new (LaughNode *node)
{
    LaughRoleTiming *segment = g_new0 (LaughRoleTiming, 1);

    segment->role.type = LaughRoleTypeTiming;
    segment->node = node;

    return segment;
}

void laugh_timing_role_source_disconnect (LaughRoleTiming *role)
{
    if (role->begin)
        _laugh_timing_source_disconnect (role->begin);
    if (role->dur)
        _laugh_timing_source_disconnect (role->dur);
    if (role->end)
        _laugh_timing_source_disconnect (role->end);
}

void laugh_timing_role_delete (LaughRoleTiming *segment)
{
    LaughRoleTiming *parent = segment->parent;

    if (segment->begin)
        laugh_timing_delete (segment->begin);
    if (segment->dur)
        laugh_timing_delete (segment->dur);
    if (segment->end)
        laugh_timing_delete (segment->end);

    if (segment->sub_segments)
        g_printerr ("TimingSegment %s leaking sub segments\n",
                laugh_tag_from_id (segment->node->id));

    if (parent) {
        GSList *s = parent->sub_segments;
        for ( ;s; s = s->next) {
            GSList *thread = (GSList *) s->data;
            if (g_slist_find (thread, segment)) {
                thread = g_slist_remove (thread, segment);
                if (!thread)
                    parent->sub_segments = g_slist_remove (parent->sub_segments,
                                                           s->data);
                else
                    s->data = thread;
                break;
            }
        }
    }
    g_free (segment);
}

LaughTiming *laugh_timing_new_from_string (const gchar *value)
{
    LaughTiming *t = laugh_timing_new ();
    if (laugh_timing_set_value (t, value))
        return t;
    laugh_timing_delete (t);
    return NULL;
}

void laugh_timing_role_child_add (LaughRoleTiming *s, LaughRoleTiming *child)
{
    if (!s->sub_segments || LaughTagIdPar == s->node->id) {
        s->sub_segments = g_slist_append (s->sub_segments,
                                          g_slist_append (NULL, child));
    } else {
        GSList *last = g_slist_last (s->sub_segments);
        last->data = g_slist_append ((GSList *) last->data, child);
    }

    child->parent = s;
}

gboolean laugh_timing_setting_set_attribute (LaughRoleTiming *segment,
        LaughNodeAttributeId att, const gchar *val)
{
    LaughTiming **timing;

    switch (att) {
        case LaughAttrIdBegin:
            timing = &segment->begin;
            break;
        case LaughAttrIdDur:
            timing = &segment->dur;
            break;
        case LaughAttrIdEnd:
            timing = &segment->end;
            break;
        default:
            return FALSE;
    }
    if (*timing) {
        if (val) {
            laugh_timing_set_value (*timing, val);
        } else {
            laugh_timing_delete (*timing);
            *timing = NULL;
        }
    } else if (val) {
        *timing = laugh_timing_new_from_string (val);
    }

    return TRUE;
}

void _laugh_timing_stop (LaughTiming *timing)
{
    if (timing)
        _laugh_timing_source_disconnect (timing);
}

static void _laugh_timing_role_dump (LaughRoleTiming *segment)
{
    GSList *s = segment->sub_segments;
    gboolean first = TRUE;

    g_printf ("%s %d", laugh_tag_from_id (segment->node->id), segment->node->state);

    if (s) {
        g_printf ("[");
        for ( ; s; s = s->next) {
            GSList *s1 = (GSList *) s->data;
            gboolean sub_first = TRUE;
            if (!first)
                g_printf (", ");
            else
                first = FALSE;
            for ( ; s1; s1 = s1->next) {
                if (!sub_first)
                    g_printf ("->");
                else
                    sub_first = FALSE;
                _laugh_timing_role_dump ((LaughRoleTiming *) s1->data);
            }
        }
        g_printf ("]");
    }
}

static
void _laugh_timing_setting_stop_freeze (LaughRoleTiming *role, LaughNodeState to_state)
{
    GSList *s;

    _laugh_timing_stop (role->begin);
    _laugh_timing_stop (role->dur);
    _laugh_timing_stop (role->end);

    laugh_timing_role_source_disconnect (role);

    /*TODO determine freeze */
    for (s = role->sub_segments; s; s = s->next) {
        GSList *sub;
        for (sub = (GSList *) s->data; sub; sub = sub->next) {
            LaughRoleTiming *seg = (LaughRoleTiming *) sub->data;

            laugh_timing_role_source_disconnect (seg);

            switch (to_state) {
                case LaughStateStopped:
                    if (seg->active || LaughStateFreezed == seg->node->state)
                        laugh_timing_setting_stop (seg);
                    break;
                case LaughStateFreezed:
                    if (seg->active)
                        /*FIXME: check fill*/
                        laugh_timing_setting_stop (seg);
                    break;
                case LaughStateInitialized:
                    if (seg->node->state > LaughStateInitialized &&
                            seg->node->state != LaughStateStopped) {
                        _laugh_timing_setting_stop_freeze (seg, to_state);
                        LaughNodeClass *klass = LAUGH_NODE_GET_CLASS(seg->node);
                        if (klass->stop)
                            klass->stop (seg->node);
                    }
                    seg->node->state = LaughStateInitialized;
                    seg->active = FALSE;
                    break;
                default:
                    break;
            }
        }
    }

    role->stop_time_stamp = clutter_get_timestamp ();

    role->active = FALSE;
}

static void _laugh_timing_setting_stopped (LaughRoleTiming *segment)
{
    GSList *s;
    LaughRoleTiming *parent = segment->parent;
    gboolean last_one = TRUE;
    LaughRoleTiming *start_next = NULL;
    gboolean freeze = FALSE;
    gboolean found = FALSE;

    g_printf ("laugh_timing_setting_stopped %s\n",
            laugh_tag_from_id (segment->node->id));

    if (parent) {
        for (s = parent->sub_segments; s; s = s->next) {
            GSList *sub = (GSList *) s->data;
            GSList *l = found ? NULL : g_slist_find (sub, segment);
            if (l) {
                found = TRUE;
                if (l->next) {
                    start_next = (LaughRoleTiming*) l->next->data;
                    last_one = FALSE;
                } else {
                    const gchar *fill = laugh_node_get_attribute (
                            segment->node, LaughAttrIdFill);
                    if ((!fill && !segment->dur && !segment->end) ||
                            (fill && !strcmp (fill, "freeze")) ||
                            (fill && !strcmp (fill, "hold")))
                        freeze = TRUE;
                }
            } else if (last_one) {
                LaughRoleTiming *seg =
                    (LaughRoleTiming *) g_slist_last (sub)->data;
                last_one = seg->node->state >= LaughStateStopped;
            }
            if (found && !last_one)
                break;
        }

        if (freeze) {
            _laugh_timing_setting_stop_freeze (segment, LaughStateFreezed);
            laugh_node_freeze (segment->node);
        } else {
            laugh_timing_setting_stop (segment);
        }

        if (start_next)
            laugh_timing_setting_start (start_next);
        else if (last_one &&
                (!parent->dur ||
                    (parent->dur->type == LaughTimingTime && !parent->dur->timeout_id)))
            _laugh_timing_setting_stopped (parent);
        else if (freeze)
            g_printf ("freeze %s\n", laugh_tag_from_id (segment->node->id));

    } else {
        laugh_timing_setting_stop (segment);
    }
}

static void _laugh_timing_element_synced (LaughNode *node, LaughRoleTiming *timing_role)
{
    LaughTiming *timing = NULL;

    if (timing_role->begin && timing_role->begin->handler_id)
        timing = timing_role->begin;
    else if (timing_role->end && timing_role->end->handler_id)
        timing = timing_role->end;

    if (!timing) {
        g_printerr ("spurious element started event\n");
    } else {
        _laugh_timing_source_disconnect (timing);

        _laugh_timing_time_connect (timing, timing->offset, timing_role);
    }
}

static void
_laugh_timing_role_sync_on (LaughRoleTiming *role, LaughTiming *timing)
{
    LaughNode *target;
    LaughRoleTiming *target_role;
    LaughTimingType type = timing->type;
    LaughNodeState required_state;
    const gchar *signal;
    glong offset;

    if (!timing->element_id) {
        g_printerr ("Sync on element not specified\n");
        return ;
    }

    target = laugh_document_get_element_by_id (role->node->document,
            laugh_document_id_from_string (role->node->document, timing->element_id));
    if (!target) {
        g_printerr ("Sync on element %s: not found\n", timing->element_id);
        return ;
    }

    target_role = (LaughRoleTiming *) laugh_node_role_get (target, LaughRoleTypeTiming);
    if (!target_role) {
        g_printerr ("Sync on element %s: has no timing\n", timing->element_id);
        return ;
    }

    switch (type) {
        case LaughTimingStartSync:
            signal = "started";
            required_state = LaughStateBegun;
            offset = (clutter_get_timestamp () - target_role->start_time_stamp) / 1000;
            break;
        case LaughTimingEndSync:
            signal = "stopped";
            required_state = LaughStateStopped;
            offset = (clutter_get_timestamp () - target_role->stop_time_stamp) / 1000;
            break;
        default:
            g_printerr ("TODO: connection to element event %d\n", type);
            return;
    }

    if (target->state >= required_state) {
        _laugh_timing_time_connect (timing, timing->offset - offset, role);
    } else {
        /*TODO: handle neg. offset */
        _laugh_timing_source_connect (timing, target, signal,
                (GCallback)_laugh_timing_element_synced, role);
    }
}

static void _laugh_timing_setting_started (LaughRoleTiming *timing_role)
{
    LaughTiming *dur = timing_role->dur;
    GSList *s;

    g_printf ("laugh_timing_setting_started %s\n",
            laugh_tag_from_id (timing_role->node->id));
    laugh_node_start (timing_role->node);
    if (dur && LaughTimingMedia == dur->type)
        g_printf ("laugh_timing_setting_started LaughTimingMedia\n");

    if (dur && LaughTimingTime == dur->type)
        _laugh_timing_time_connect (dur, dur->offset, timing_role);
    else if (timing_role->end)
        _laugh_timing_role_sync_on (timing_role, timing_role->end);

    if (timing_role->active) {
        if (timing_role->sub_segments) {
            for (s = timing_role->sub_segments; s && timing_role->active; s = s->next)
                laugh_timing_setting_start (
                        (LaughRoleTiming *)((GSList *) s->data)->data);
        } else if (!timing_role->dur && !timing_role->end) {
            _laugh_timing_setting_stopped (timing_role);
        }
    }
}

static gboolean _laugh_timing_timeout (gpointer data)
{
    LaughRoleTiming *segment = (LaughRoleTiming *) data;

    if (segment->begin && segment->begin->timeout_id) {
        laugh_timing_notify (segment, segment->begin->timeout_id);
        segment->begin->timeout_id = 0;
    } else if (segment->dur && segment->dur->timeout_id) {
        laugh_timing_notify (segment, segment->dur->timeout_id);
        segment->dur->timeout_id = 0;
    } else if (segment->end && segment->end->timeout_id) {
        laugh_timing_notify (segment, segment->end->timeout_id);
        segment->end->timeout_id = 0;
    }

    return FALSE; /*TODO handle repeat*/
}

gboolean laugh_timing_setting_start (LaughRoleTiming *timing_role)
{
    LaughTimingType timing_type = LaughTimingTime;
    int offset = 0;

    g_printf ("laugh_timing_setting_start %s\n",
            laugh_tag_from_id (timing_role->node->id));
    timing_role->active = TRUE;
    timing_role->start_time_stamp = clutter_get_timestamp ();

    if (!timing_role->begin)
        timing_role->begin = laugh_timing_new ();

    timing_type = timing_role->begin->type;
    offset = timing_role->begin->offset;

    if (LaughTimingTime == timing_type)
        _laugh_timing_time_connect (timing_role->begin, offset, timing_role);
    else
        _laugh_timing_role_sync_on (timing_role, timing_role->begin);

    return TRUE;
}

void laugh_timing_notify (LaughRoleTiming *segment, gulong timeout_id)
{
    if (segment->begin && segment->begin->timeout_id == timeout_id) {
        _laugh_timing_setting_started (segment);
    } else if ((segment->dur &&
                (segment->dur->timeout_id == timeout_id ||
                 segment->dur->handler_id == timeout_id)) ||
            (segment->end && segment->end->timeout_id == timeout_id)) {
        _laugh_timing_setting_stopped (segment);
    }
}

void laugh_timing_setting_stop (LaughRoleTiming *segment)
{
    g_printf ("laugh_timing_setting_stop %s\n",
            laugh_tag_from_id (segment->node->id));

    _laugh_timing_setting_stop_freeze (segment, LaughStateStopped);

    laugh_node_stop (segment->node);
}

static LaughRoleTiming *
_laugh_timing_settting_find (LaughRoleTiming *segment, const LaughNode *node)
{
    if (segment->node == node)
        return segment;
    if (segment->sub_segments) {
        GSList *s;
        for (s = segment->sub_segments; s; s = s->next) {
            GSList *sub;
            for (sub = (GSList *) s->data; sub; sub = sub->next) {
                LaughRoleTiming *seg = _laugh_timing_settting_find (
                        (LaughRoleTiming *) sub->data, node);
                if (seg)
                    return seg;
            }
        }
    }
    return NULL;
}

static void _laugh_timing_settting_sub_segments_force_stop (LaughRoleTiming *segment)
{
    GSList *s;

    for (s = segment->sub_segments; s; s = s->next) {
        GSList *sub;
        for (sub = (GSList *) s->data; sub; sub = sub->next) {
            LaughRoleTiming *seg = (LaughRoleTiming *) sub->data;
            if (seg->node->state > LaughStateInitialized) {
                _laugh_timing_setting_stop_freeze (seg, LaughStateInitialized);
                if (seg->node->state > LaughStateInitialized &&
                        seg->node->state != LaughStateStopped) {
                    LaughNodeClass *klass = LAUGH_NODE_GET_CLASS(seg->node);
                    if (klass->stop)
                        klass->stop (seg->node);
                }
                seg->node->state = LaughStateInitialized;
            } else {
                break;
            }
        }
    }
}

static void
_laugh_timing_settting_recursive_start (LaughRoleTiming *role, LaughRoleTiming *child)
{
    if (!role) {
        g_printerr ("jump target starting underflow error\n");
        return;
    }
    if (child && LaughTagIdPar != role->node->id) {
        GSList *s;

        for (s = role->sub_segments; s; s = s->next) {
            GSList *seg;
            if (g_slist_find ((GSList *)s->data, child)) {
                for (seg = (GSList *)s->data; seg; seg = seg->next) {
                    LaughRoleTiming *r = (LaughRoleTiming *)seg->data;
                    if (r == child)
                        break;
                    r->node->state = LaughStateStopped;
                }
                break;
            }
        }
    }

    if (!role->active) {
        _laugh_timing_settting_recursive_start (role->parent, role);
        if (LaughStateInitialized >= role->node->state)
            laugh_timing_setting_start (role);
    }
}

void laugh_timing_setting_jump (LaughNode *target)
{
    LaughRoleTiming *segment = target->document->timing;
    LaughRoleTiming *seg;

    if (LaughStateBegun == target->state) {
        g_printerr ("jump target already active\n");
        return;
    }

    segment = _laugh_timing_settting_find (segment, target);
    if (!segment) {
        g_printerr ("jump target not in tree\n");
        return;
    }

    for (seg = segment->parent; seg; seg = seg->parent)
        if (seg->active) {
            g_printf ("laugh_timing_setting_jump force stop %s\n",
                    laugh_tag_from_id (seg->node->id));

            if (LaughTagIdPar != seg->node->id)
                _laugh_timing_settting_sub_segments_force_stop (seg);
            _laugh_timing_settting_recursive_start (segment, NULL);

            break;
        }
}

LaughNode *laugh_timing_container_new (LaughDocument *doc, LaughNodeTagId id,
        GHashTable *attributes)
{
    LaughNode *n = (LaughNode *)g_object_new(LAUGH_TYPE_TIMING_CONTAINER, NULL);

    laugh_node_base_construct (doc, n, id, attributes);

    return n;
}

int laugh_timing_set_value (LaughTiming *timing, const gchar *value)
{
    return _get_timing (value, timing);
}

/*#define TIMING_TEST*/

#ifdef TIMING_TEST

/* gcc laugh-timing.c -o timing-test `pkg-config --cflags --libs glib-2.0` -DTIMING_TEST*/

#define TEST_PARSE_TIME(s)                                       \
{                                                                \
    int val = 0;                                                 \
    int b = _timing_parse_time(s, &val);                          \
    g_printf( "_timing_parse_time(%s) => %d: %d\n", (s), b, val); \
}

const char *timing_string (LaughTimingType tt) {
    switch (tt) {
        case LaughTimingUnknown:
            return "unknown";
        case LaughTimingTime:
            return "timer";
        case LaughTimingIndefinite:
            return "infinite";
        case LaughTimingMedia:
            return "media";
        case LaughTimingActivated:
            return "activated";
        case LaughTimingInbounds:
            return "inbounds";
        case LaughTimingOutbounds:
            return "outbounds";
        case LaughTimingEndSync:
            return "endSync";
        case LaughTimingStartSync:
            return "startSync";
        default:
            break;
    }
    return "error";
}

#define TEST_GET_TIMING(s)                                          \
{                                                                   \
    LaughTiming *t = laugh_timing_new_from_string(s);               \
    g_printf( "_get_timing %s => target %s event %s offset%d\n",     \
            (s), t->element_id, timing_string(t->type), t->offset); \
    laugh_timing_delete (t);                                        \
}

int main () {
    TEST_PARSE_TIME("12");
    TEST_PARSE_TIME("12s");
    TEST_PARSE_TIME("12m");
    TEST_PARSE_TIME("12h");
    TEST_PARSE_TIME("12.4");
    TEST_PARSE_TIME("12.4s");
    TEST_PARSE_TIME("12.4m");
    TEST_PARSE_TIME("12.4h");
    TEST_PARSE_TIME(".4");
    TEST_PARSE_TIME(".4s");
    TEST_PARSE_TIME("+.4");
    TEST_PARSE_TIME("+.4s");
    TEST_PARSE_TIME("-.4");
    TEST_PARSE_TIME("-.4s");
    TEST_GET_TIMING("4");
    TEST_GET_TIMING("myimage.activateEvent");
    TEST_GET_TIMING("myimage.activateEvent+2");
    TEST_GET_TIMING("myimage.endSync");
    TEST_GET_TIMING("myimage.endSync-2m");
    TEST_GET_TIMING("myimage.begin-2");
    TEST_GET_TIMING("MyImage.endSync");
    TEST_GET_TIMING("id(MyImage)(endSync)");
    TEST_GET_TIMING("indefinite");
    return 0;
}

#endif
