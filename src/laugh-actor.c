/*
 * Laugh.
 *
 * An glib SMIL library.
 *
 * Authored By Koos Vriezen  <koos.vriezen@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <string.h>
#include <glib/gprintf.h>

#include "laugh-actor.h"
#include "laugh-dom.h"

#define LAUGH_ACTOR_GET_PRIVATE(obj) \
(G_TYPE_INSTANCE_GET_PRIVATE ((obj), LAUGH_TYPE_ACTOR, LaughActorPrivate))


enum
{
    PROP_0,

    PROP_LOADED,
    PROP_FINISHED,
};

struct _LaughActorPrivate
{
    LaughDocument *document;
    guint width;
    guint height;
    gboolean loaded;
    gboolean finished;
};

static gpointer laugh_actor_parent_class = ((void *)0);

static void laugh_actor_finalize (GObject *object)
{
    LaughActor *self = LAUGH_ACTOR(object);

    if (self->priv->document)
        g_object_unref (G_OBJECT (self->priv->document));

    G_OBJECT_CLASS (laugh_actor_parent_class)->finalize (object);
}

static void laugh_actor_dispose (GObject *object)
{
    G_OBJECT_CLASS (laugh_actor_parent_class)->dispose (object);
}

static void laugh_actor_get_property (GObject *object, guint prop_id,
        GValue *value, GParamSpec *pspec)
{
    LaughActor *self = LAUGH_ACTOR(object);
    LaughActorPrivate *priv = self->priv;

    switch (prop_id) {
        case PROP_LOADED:
            g_value_set_boolean (value, priv->loaded);
            break;
        case PROP_FINISHED:
            g_value_set_boolean (value, priv->finished);
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
            break;
    }
}

static void
_laugh_actor_request_coords (ClutterActor *self, ClutterActorBox *box)
{
    LaughActor *actor = LAUGH_ACTOR(self);

    actor->priv->width =
        CLUTTER_UNITS_TO_INT ((box->x2)) - CLUTTER_UNITS_TO_INT ((box->x1));
    actor->priv->height =
        CLUTTER_UNITS_TO_INT ((box->y2)) - CLUTTER_UNITS_TO_INT ((box->y1));
    g_printf ("resizing %dx%d\n", actor->priv->width, actor->priv->height);

    CLUTTER_ACTOR_CLASS (laugh_actor_parent_class)->request_coords (self, box);
}

static void laugh_actor_class_init (LaughActorClass *klass)
{
    GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
    ClutterActorClass *actor_class = (ClutterActorClass *) klass;

    laugh_actor_parent_class = g_type_class_peek_parent (klass);

    gobject_class->finalize = laugh_actor_finalize;
    gobject_class->dispose = laugh_actor_dispose;
    gobject_class->get_property = laugh_actor_get_property;
    actor_class->request_coords = _laugh_actor_request_coords;

    g_object_class_install_property (gobject_class,
            PROP_LOADED,
            g_param_spec_boolean ("loaded",
                "Loaded",
                "Whether the SMIL referenced media objects are in memory",
                FALSE,
                G_PARAM_READABLE | G_PARAM_STATIC_NAME |
                G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB));

    g_object_class_install_property (gobject_class,
            PROP_FINISHED,
            g_param_spec_boolean ("finished",
                "Finished",
                "Whether playing the SMIL document is finished",
                FALSE,
                G_PARAM_READABLE | G_PARAM_STATIC_NAME |
                G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB));

    g_type_class_add_private (gobject_class, sizeof (LaughActorPrivate));
}

static
void laugh_actor_instance_init (GTypeInstance *instance, gpointer g_class)
{
    LaughActor *self = (LaughActor *)instance;

    self->priv = LAUGH_ACTOR_GET_PRIVATE (self);
}

GType laugh_actor_get_type (void)
{
    static GType type = 0;
    if (type == 0) {
        static const GTypeInfo info = {
            sizeof (LaughActorClass),
            NULL,   /* base_init */
            NULL,   /* base_finalize */
            (GClassInitFunc) laugh_actor_class_init,   /* class_init */
            NULL,   /* class_finalize */
            NULL,   /* class_data */
            sizeof (LaughActor),
            0,      /* n_preallocs */
            laugh_actor_instance_init    /* instance_init */
        };
        type = g_type_register_static (CLUTTER_TYPE_GROUP,
                "LaughActorType",
                &info, 0);
    }
    return type;
}

ClutterActor *laugh_actor_new ()
{
    ClutterActor *actor = (ClutterActor *)g_object_new(LAUGH_TYPE_ACTOR, NULL);

    return actor;
}

void dump_timings (LaughRoleTiming *segment)
{
    GSList *s = segment->sub_segments;
    gboolean first = TRUE;

    g_printf ("%s", laugh_tag_from_id (segment->node->id));

    if (s) {
        g_printf ("[");
        for ( ; s; s = s->next) {
            GSList *s1 = (GSList *) s->data;
            gboolean sub_first = TRUE;
            if (!first)
                g_printf (", ");
            else
                first = FALSE;
            for ( ; s1; s1 = s1->next) {
                if (!sub_first)
                    g_printf ("->");
                else
                    sub_first = FALSE;
                dump_timings ((LaughRoleTiming *) s1->data);
            }
        }
        g_printf ("]");
    }
}

void _laugh_actor_initialized (LaughNode *node, gpointer d)
{
    LaughActor *self = LAUGH_ACTOR (d);
    gchar *xml = laugh_node_get_inner_xml (node);

    g_printf ("LaughActor initialized\n%s\n", xml);
    dump_timings (((LaughDocument *)node)->timing);
    g_printf ("\n");

    g_free (xml);

    self->priv->loaded = TRUE;
    g_object_notify (G_OBJECT (self), "loaded");
}

void _laugh_actor_stopped (LaughNode *node, gpointer d)
{
    LaughActor *self = LAUGH_ACTOR (d);

    g_printf ("LaughActor stopped\n");
    self->priv->finished = TRUE;
    g_object_notify (G_OBJECT (self), "finished");
}

void laugh_actor_uri_set (LaughActor *actor, const gchar *uri)
{
    LaughActorPrivate *priv = actor->priv;

    if (!priv->document) {
        priv->document = laugh_document_new (uri);

        g_signal_connect (G_OBJECT (priv->document), "initialized",
                (GCallback) _laugh_actor_initialized, actor);
        g_signal_connect (G_OBJECT (priv->document), "stopped",
                (GCallback) _laugh_actor_stopped, actor);
    } else {
        laugh_document_set_uri (priv->document, uri);
    }
}

void laugh_actor_start (LaughActor *actor)
{
    LaughActorPrivate *priv = actor->priv;

    priv->loaded = FALSE;
    priv->finished = FALSE;

    laugh_document_open (priv->document, CLUTTER_CONTAINER (actor),
            priv->width, priv->height);
}
