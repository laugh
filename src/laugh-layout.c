/*
 * Laugh.
 *
 * An glib SMIL library.
 *
 * Authored By Koos Vriezen  <koos.vriezen@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/**
 * SECTION:laugh-layout
 * @short_description: DOM layout classes.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "laugh-layout.h"
#include "laugh-io.h"

#include <string.h>
#include <stdlib.h>
#include <glib/gprintf.h>
#include <clutter/clutter-group.h>
#include <clutter/clutter-rectangle.h>

#define LAUGH_LAYOUT_GET_PRIVATE(obj) \
(G_TYPE_INSTANCE_GET_PRIVATE ((obj), LAUGH_TYPE_LAYOUT, LaughLayoutPrivate))


struct _LaughLayoutPrivate
{
    LaughRoleDisplay *display_role;
    LaughRoleDisplayGroup *display_group_role;
    ClutterColor bg_color;
    int zindex;
    int order;
    int parent_width;
    int parent_height;
    LaughIO *io;
};

static gpointer laugh_layout_parent_class = ((void *)0);

static void laugh_layout_finalize (GObject *object)
{
    G_OBJECT_CLASS (laugh_layout_parent_class)->finalize (object);
}

static void laugh_layout_dispose (GObject *object)
{
    LaughLayout *layout = LAUGH_LAYOUT(object);
    LaughLayoutPrivate *priv = layout->priv;

    if (priv->display_role->actor)
        clutter_actor_destroy (priv->display_role->actor);
    if (priv->display_group_role->actor)
        clutter_actor_destroy (priv->display_group_role->actor);
    g_free (priv->display_role);
    g_free (priv->display_group_role);

    G_OBJECT_CLASS (laugh_layout_parent_class)->dispose (object);
}

void _lauch_layout_mime_type (LaughIO *io, const gchar *mime, gpointer d)
{
    g_printf ("lauch_layout_mime_type: %s\n", mime);
    if (strncmp (mime, "image/", 6)) {
        LaughNode *node = LAUGH_NODE (d);

        g_printerr ("region background not an image\n");
        laugh_io_cancel (io);

        g_object_unref (G_OBJECT (io));

        laugh_node_base_emit_initialized (node);
    }
}

static void
_lauch_layout_completed (LaughIO *io, gsize sz, gpointer data, gpointer d)
{
    LaughNode *node = LAUGH_NODE (d);

    g_printf ("_lauch_layout_completed: %u\n", sz);
    if (data) {
        /* create Pixbuf and ClutterActor*/

        g_free (data);
    }
    g_object_unref (G_OBJECT (io));

    laugh_node_base_emit_initialized (node);
}

static void _lauch_layout_actor_destroyed (ClutterActor *actor, gpointer data)
{
    LaughLayout *self = LAUGH_LAYOUT(data);
    LaughLayoutPrivate *priv = self->priv;

    if (actor == priv->display_role->actor)
        priv->display_role->actor = NULL;
    else if (actor == priv->display_group_role->actor)
        priv->display_group_role->actor = NULL;
}

static void
_laugh_layout_create_background (LaughLayout *layout, guint w, guint h)
{
    LaughLayoutPrivate *priv = layout->priv;
    LaughRoleDisplay *display_role = priv->display_role;

    if (display_role->actor)
        clutter_actor_destroy (display_role->actor);

    /*if (priv->bg_color.alpha > 0) {*/
        display_role->actor = clutter_rectangle_new ();
        clutter_rectangle_set_color (
                CLUTTER_RECTANGLE (display_role->actor),
                &priv->bg_color);
        clutter_actor_set_size (display_role->actor, w, h);
        clutter_actor_set_position (display_role->actor, 0, 0);
        clutter_container_add_actor (CLUTTER_CONTAINER (priv->display_group_role->actor),
                display_role->actor);
        clutter_actor_show (display_role->actor);
        g_signal_connect (G_OBJECT (display_role->actor), "destroy",
                (GCallback) _lauch_layout_actor_destroyed, layout);
    /*} else {
        priv->background_actor = NULL;
    }*/
}

static gint _laugh_layout_region_sort (gconstpointer a, gconstpointer b)
{

    const LaughLayout *r1 = *(LaughLayout **) a;
    const LaughLayout *r2 = *(LaughLayout **) b;

    if (r1->priv->zindex == r2->priv->zindex)
        return r1->priv->order - r2->priv->order;
    return r1->priv->zindex - r2->priv->zindex;
}

static void _laugh_layout_zindex_apply (LaughNode *node)
{
    LaughLayout *self = (LaughLayout *) node;
    LaughLayoutPrivate *priv = self->priv;
    LaughNode *child;
    GArray *regions = g_array_new (FALSE, FALSE, sizeof (LaughNode *));
    int i, size = 0;

    for (child = node->first_child; child; child = child->next_sibling)
        if (LaughTagIdRegion == child->id) {
            g_array_append_val (regions, child);
            ((LaughLayout *) child)->priv->order = size++;
        }

    if (size > 1) {
        ClutterContainer *container = CLUTTER_CONTAINER (priv->display_group_role->actor);
        g_array_sort (regions, _laugh_layout_region_sort);

        for (i = 0; i < size -1; ++i) {
            LaughRoleDisplayGroup *group1 = (LaughRoleDisplayGroup *)
                laugh_node_role_get (g_array_index (regions, LaughNode*, i),
                        LaughRoleTypeDisplayGroup);
            LaughRoleDisplayGroup *group2 = (LaughRoleDisplayGroup *)
                laugh_node_role_get (g_array_index (regions, LaughNode*, i+1),
                        LaughRoleTypeDisplayGroup);
            clutter_container_raise_child (container, group2->actor, group1->actor);
        }
        clutter_actor_queue_redraw ((ClutterActor *) container);
    }

    g_array_free (regions, TRUE);
}

static
void _lauch_layout_actor_added (ClutterContainer *con, ClutterActor *actor, gpointer data)
{
    LaughLayout *self = (LaughLayout *) data;
    clutter_container_raise_child (con, actor, self->priv->display_role->actor);
}

static void _laugh_layout_init (LaughNode *node, LaughInitializer *initializer)
{
    LaughLayout *self = (LaughLayout *) node;
    LaughLayoutPrivate *priv = self->priv;
    LaughNode *child;
    ClutterActor *region_actor;
    ClutterContainer *parent_actor = initializer->parent_region;

    priv->parent_width = initializer->parent_width;
    priv->parent_height = initializer->parent_height;

    if (node->attributes)
        g_hash_table_foreach (node->attributes, laugh_attributes_set, node);

    if (parent_actor) {
        float x, y, w, h;

        region_actor = clutter_group_new ();
        priv->display_group_role->actor = region_actor;
        g_signal_connect (G_OBJECT (region_actor), "destroy",
                (GCallback) _lauch_layout_actor_destroyed, self);

        if (LaughTagIdLayout != node->id) {
            laugh_size_setting_get (&self->size_setting,
                    priv->parent_width, priv->parent_height, &x, &y, &w, &h);
            initializer->parent_width = w;
            initializer->parent_height = h;
            clutter_actor_set_size (region_actor, w, h);
            clutter_actor_set_position (region_actor, x, y);
            clutter_actor_set_clip (region_actor, 0, 0, w, h);

            _laugh_layout_create_background (self, w, h);
        }

        switch (node->id) {
            case LaughTagIdRootLayout:
                if (node->parent_node &&
                        LaughTagIdLayout == node->parent_node->id) {
                    LaughNode *layout = node->parent_node;
                    LaughRoleDisplayGroup *group = (LaughRoleDisplayGroup *)
                        laugh_node_role_get (layout, LaughRoleTypeDisplayGroup);
                    float xscale = priv->parent_width / w;
                    float yscale = priv->parent_height / h;
                    clutter_actor_set_clip (group->actor, 0, 0, w, h);
                    if (xscale > yscale)
                        clutter_actor_set_scale (group->actor, yscale, yscale);
                    else
                        clutter_actor_set_scale (group->actor, xscale, xscale);
                    break;
                }
            case LaughTagIdRegion:
            default:
                break;
        }
        clutter_container_add_actor (parent_actor, region_actor);
        clutter_actor_show (region_actor);

        initializer->parent_region = CLUTTER_CONTAINER (region_actor);
    }

    for (child = node->first_child; child; child = child->next_sibling)
        laugh_node_init (child, initializer);

    if (parent_actor)
        g_signal_connect (G_OBJECT (region_actor), "actor-added",
                (GCallback) _lauch_layout_actor_added, self);

    initializer->parent_region = parent_actor;
    if (LaughTagIdRootLayout != node->id) {
        initializer->parent_width = priv->parent_width;
        initializer->parent_height = priv->parent_height;
    }

    _laugh_layout_zindex_apply (node);

    if (!priv->io)
        node->state = LaughStateInitialized;
}

static void _laugh_layout_sizes_update (LaughNode *node, gint pw, gint ph)
{
    if (LaughTagIdRegion == node->id) {
        LaughLayout *layout = LAUGH_LAYOUT (node);
        LaughLayoutPrivate *priv = layout->priv;
        float x, y, w, h;
        LaughNode *child;
        guint width, height;

        clutter_actor_get_size (priv->display_role->actor, &width, &height);

        priv->parent_width = pw;
        priv->parent_height = ph;

        laugh_size_setting_get (&layout->size_setting, pw, ph, &x, &y, &w, &h);
        clutter_actor_set_size (priv->display_group_role->actor, w, h);
        clutter_actor_set_position (priv->display_group_role->actor, x, y);
        clutter_actor_set_clip (priv->display_group_role->actor, 0, 0, w, h);
        clutter_actor_set_size (priv->display_role->actor, w, h);

        if (width != (guint) w || height != (guint) h)
            for (child = node->first_child; child; child = child->next_sibling)
                _laugh_layout_sizes_update (child, w, h);
    }
}

static void _laugh_layout_set_attribute (LaughNode *node,
        LaughNodeAttributeId att, const gchar *val, gpointer *undo)
{
    const gchar *value = val;
    gboolean need_sizing = FALSE;
    LaughLayout *self = (LaughLayout *)node;
    LaughLayoutPrivate *priv = self->priv;

    LAUGH_TYPE_NODE_CLASS(laugh_layout_parent_class)->
        set_attribute(node, att, val, undo);

    if (!val && undo)
        value = *(const gchar **)undo;
    /*g_printf ("_laugh_layout_set_attribute %s=%s\n", laugh_attribute_from_id (att), value);*/

    if (laugh_size_setting_set_attribute (&self->size_setting, att, value)) {
        need_sizing = TRUE;
    } else {
        switch (att) {
            case LaughAttrIdBgColor:
            case LaughAttrIdBgColor1:
                /*TODO destory existing CluttorActor */
                /*TODO create CluttorActor */
                if (!value || !clutter_color_parse (value, &priv->bg_color)) {
                    if (value)
                        g_printerr ("parse color %s failed\n", value);
                    priv->bg_color.alpha = 0;
                }
                if (node->state > LaughStateInit) {
                    clutter_rectangle_set_color (
                            CLUTTER_RECTANGLE (priv->display_role->actor),
                            &priv->bg_color);
                }
                break;
            case LaughAttrIdBgImage:
                /*TODO destory existing CluttorActor */
                if (value) {
                    self->priv->io = laugh_io_new (value, NULL);

                    g_signal_connect (G_OBJECT (self->priv->io), "mime-type",
                            (GCallback) _lauch_layout_mime_type, (gpointer) node);
                    g_signal_connect (G_OBJECT (self->priv->io), "completed",
                            (GCallback) _lauch_layout_completed, (gpointer) node);

                    laugh_io_open (self->priv->io);
                }
                break;
            case LaughAttrIdZIndex: {
                char *endptr = NULL;
                self->priv->zindex = value ? strtol (value, &endptr, 10) : 0;
                if (endptr == value)
                    self->priv->zindex = 0;
                if (node->state > LaughStateInit && node->parent_node)
                    _laugh_layout_zindex_apply (node->parent_node);
                break;
            }
            case LaughAttrIdName:
                if (!node->element_id)
                    node->element_id = laugh_document_id_from_string (
                            node->document, val);
                break;
                /*TODO showBackground */
            default:
                break; /* kill warning: enumeration value  not handled in switch*/
        }
    }
    if (node->state > LaughStateInit && need_sizing)
        _laugh_layout_sizes_update (node,
                priv->parent_width, priv->parent_height);
}

static LaughRole *_laugh_layout_role (LaughNode *node, LaughRoleType type)
{
    LaughLayout *self = (LaughLayout *) node;

    switch (type) {
        case LaughRoleTypeDisplay:
            return (LaughRole *) self->priv->display_role;
        case LaughRoleTypeDisplayGroup:
            return (LaughRole *) self->priv->display_group_role;
        default:
            return NULL;
    }
}

static void laugh_layout_class_init (LaughLayoutClass *klass)
{
    GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
    LaughNodeClass *node_class = (LaughNodeClass *) klass;

    laugh_layout_parent_class = g_type_class_peek_parent (klass);

    gobject_class->finalize = laugh_layout_finalize;
    gobject_class->dispose = laugh_layout_dispose;
    node_class->init = _laugh_layout_init;
    node_class->set_attribute = _laugh_layout_set_attribute;
    node_class->role = _laugh_layout_role;

    g_type_class_add_private (gobject_class, sizeof (LaughLayoutPrivate));
}

static
void laugh_layout_instance_init (GTypeInstance *instance, gpointer g_class)
{
    LaughLayout *self = (LaughLayout *)instance;
    LaughRoleDisplay *display_role;
    LaughRoleDisplayGroup *display_role_group;

    self->priv = LAUGH_LAYOUT_GET_PRIVATE (self);

    display_role = g_new0 (LaughRoleDisplay, 1);
    display_role->role.type = LaughRoleTypeDisplay;
    self->priv->display_role = display_role;

    display_role_group = g_new0 (LaughRoleDisplayGroup, 1);
    display_role_group->role.type = LaughRoleTypeDisplayGroup;
    self->priv->display_group_role = display_role_group;

    self->priv->bg_color.alpha = 0;
}

GType laugh_layout_get_type (void)
{
    static GType type = 0;
    if (type == 0) {
        static const GTypeInfo info = {
            sizeof (LaughLayoutClass),
            NULL,   /* base_init */
            NULL,   /* base_finalize */
            (GClassInitFunc) laugh_layout_class_init,   /* class_init */
            NULL,   /* class_finalize */
            NULL,   /* class_data */
            sizeof (LaughLayout),
            0,      /* n_preallocs */
            laugh_layout_instance_init    /* instance_init */
        };
        type = g_type_register_static (LAUGH_TYPE_NODE,
                "LaughLayoutType",
                &info, 0);
    }
    return type;
}

void laugh_size_set_string (LaughSize *size, const gchar *value)
{
    char *ep;
    char *p;

    if (value) {
        p = strchr (value, '%');
        if (p) {
            size->abs_size = 0.0;
            size->perc_size = strtod (value, &ep);
            size->is_set = ep != value;
        } else {
            size->abs_size = strtod (value, &ep);
            size->perc_size = 0.0;
            size->is_set = ep != value;
        }
    } else {
        size->is_set = FALSE;
    }
}

float laugh_size_get (LaughSize *size, float relative_to)
{
    return size->abs_size + size->perc_size * relative_to / 100;
}

gboolean laugh_size_setting_set_attribute (LaughSizeSetting *sizes,
                                LaughNodeAttributeId att, const gchar *value)
{
    switch (att) {
        case LaughAttrIdLeft:
            laugh_size_set_string (&sizes->left, value);
            return TRUE;
        case LaughAttrIdTop:
            laugh_size_set_string (&sizes->top, value);
            return TRUE;
        case LaughAttrIdWidth:
            laugh_size_set_string (&sizes->width, value);
            return TRUE;
        case LaughAttrIdHeight:
            laugh_size_set_string (&sizes->height, value);
            return TRUE;
        case LaughAttrIdRight:
            laugh_size_set_string (&sizes->right, value);
            return TRUE;
        case LaughAttrIdBottom:
            laugh_size_set_string (&sizes->bottom, value);
            return TRUE;
        default:
            return FALSE;
    }
}

void laugh_size_setting_get (LaughSizeSetting *sizes, float pw, float ph,
        float *x, float *y, float *w, float *h) {
    if (sizes->left.is_set) {
        *x = laugh_size_get (&sizes->left, pw);
    } else if (sizes->width.is_set) {
        if (sizes->right.is_set)
            *x = pw -
                laugh_size_get (&sizes->width, pw) -
                laugh_size_get (&sizes->right, pw);
        else
            *x = 0;
    } else {
        *x = 0;
    }

    if (sizes->top.is_set) {
        *y = laugh_size_get (&sizes->top, ph);
    } else if (sizes->height.is_set) {
        if (sizes->bottom.is_set)
            *y = ph -
                laugh_size_get (&sizes->height, ph) -
                laugh_size_get (&sizes->bottom, ph);
        else
            *y = 0;
    } else {
        *y = 0;
    }

    if (sizes->width.is_set)
        *w = laugh_size_get (&sizes->width, pw);
    else if (sizes->right.is_set)
        *w = pw - *x -laugh_size_get (&sizes->right, pw);
    else
        *w = pw - *x;
    if (*w < 0)
        *w = 0;

    if (sizes->height.is_set)
        *h = laugh_size_get (&sizes->height, ph);
    else if (sizes->bottom.is_set)
        *h = ph - *y - laugh_size_get (&sizes->bottom, ph);
    else
        *h = ph - *y;
    if (*h < 0)
        *h = 0;
}

LaughNode *laugh_layout_new (LaughDocument *doc, LaughNodeTagId id,
        GHashTable *attributes)
{
    LaughNode *node = LAUGH_NODE(g_object_new (LAUGH_TYPE_LAYOUT, NULL));

    laugh_node_base_construct (doc, node, id, attributes);

    return node;
}

#ifdef LAIGH_TEST_SIZES
/* gcc laugh-dom.c laugh-io.c laugh-layout.c -o sizestest `pkg-config --libs --cflags glib-2.0 gio-2.0 gthread-2.0` -lexpat -DLAIGH_TEST_SIZES */
void test_sizes (float pw, float ph,
        const gchar *left, const gchar *top,
        const gchar *width, const gchar *height,
        const gchar *right, const gchar *bottom)
{
    LaughSizeSetting sizes;
    float x, y, w, h;
    memset (&sizes, 0, sizeof (LaughSizeSetting));
    if (left)
        laugh_size_set_string (&sizes.left, left);
    if (top)
        laugh_size_set_string (&sizes.top, top);
    if (width)
        laugh_size_set_string (&sizes.width, width);
    if (height)
        laugh_size_set_string (&sizes.height, height);
    if (right)
        laugh_size_set_string (&sizes.right, right);
    if (bottom)
        laugh_size_set_string (&sizes.bottom, bottom);
    laugh_size_setting_get (&sizes, pw, ph, &x, &y, &w, &h);
    g_printf ("left %s top %s width %s height %s right %s bottom %s\n"
            "\t(%.1fx%.1f) => [%.1f, %.1f %.1fx%.1f]\n",
            pw, ph,
            left, top, width, height, right, bottom,
            x, y, w, h);
}

int main()
{
    test_sizes (320, 240, "50", "30", "200", "180", NULL, NULL);
    test_sizes (320, 240, "50", "30", NULL, NULL, "40", "20");
    test_sizes (320, 240, "50", "30", "50%", "60%", NULL, NULL);
    test_sizes (320, 240, "50", "30", NULL, NULL, "10%", "20%");
    test_sizes (320, 240, NULL, NULL, "50%", "20%", "10%", "10%");
    test_sizes (320, 240, NULL, NULL, NULL, "20%", "10%", "10%");
    test_sizes (320, 240, "5%", NULL, NULL, "20%", "10%", "10%");

    return 0;
}

#endif
