/*
 * Laugh.
 *
 * An glib SMIL library.
 *
 * Authored By Koos Vriezen  <koos.vriezen@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/**
 * SECTION:laugh-io
 * @short_description: A simple io abstraction.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "laugh-io.h"

#include <string.h>
#include <gio/gio.h>
#include <glib/gprintf.h>

#define LAUGH_IO_GET_PRIVATE(obj) \
(G_TYPE_INSTANCE_GET_PRIVATE ((obj), LAUGH_TYPE_IO, LaughIOPrivate))

enum
{
    MIME,
    COMPLETED,

    LAST_SIGNAL
};

static guint laugh_io_signals[LAST_SIGNAL] = { 0 };

struct _LaughIOPrivate {
    gchar *uri;
    gchar *mime;
    GFile *file;
    GInputStream *fin;
    GCancellable *cancellable;
    unsigned char *buffer;
    gsize buf_size;
    gsize buf_pos;
};

static gpointer laugh_io_parent_class = ((void *)0);

static void _laugh_io_clear_gio (LaughIOPrivate *priv, gboolean cancel) {
    if (priv->cancellable) {
        if (cancel) {
            g_cancellable_cancel (priv->cancellable);
            g_free (priv->buffer);
            priv->buffer = NULL;
        }
        g_object_unref (G_OBJECT (priv->cancellable));
        priv->cancellable = NULL;
        if (priv->fin) {
            g_object_unref (G_OBJECT (priv->fin));
            priv->fin = NULL;
        }
        if (priv->file) {
            g_object_unref (G_OBJECT (priv->file));
            priv->file = NULL;
        }
    }
}

static void laugh_io_finalize (GObject *object) {
    G_OBJECT_CLASS (laugh_io_parent_class)->finalize (object);
}

static void laugh_io_dispose (GObject *object) {
    LaughIO *laughio = LAUGH_IO(object);

    _laugh_io_clear_gio (laughio->priv, TRUE);
    if (laughio->priv->uri)
        g_free (laughio->priv->uri);
    if (laughio->priv->mime)
        g_free (laughio->priv->mime);

    G_OBJECT_CLASS (laugh_io_parent_class)->dispose (object);
}

static void laugh_io_class_init (LaughIOClass *klass) {
    GObjectClass *gobject_class = G_OBJECT_CLASS (klass);

    laugh_io_parent_class = g_type_class_peek_parent (klass);

    gobject_class->finalize = laugh_io_finalize;
    gobject_class->dispose = laugh_io_dispose;

    g_type_class_add_private (gobject_class, sizeof (LaughIOPrivate));

    laugh_io_signals[MIME] =
        g_signal_new ("mime-type",
                G_TYPE_FROM_CLASS (gobject_class),
                G_SIGNAL_RUN_LAST,
                G_STRUCT_OFFSET (LaughIOClass, mime_type),
                NULL, NULL,
                g_cclosure_marshal_VOID__STRING,
                G_TYPE_NONE, 1,
                G_TYPE_STRING);

    laugh_io_signals[COMPLETED] =
        g_signal_new ("completed",
                G_TYPE_FROM_CLASS (gobject_class),
                G_SIGNAL_RUN_LAST,
                G_STRUCT_OFFSET (LaughIOClass, completed),
                NULL, NULL,
                g_cclosure_marshal_VOID__UINT_POINTER,
                G_TYPE_NONE, 2,
                G_TYPE_UINT, G_TYPE_POINTER);
}

static void laugh_io_instance_init (GTypeInstance *instance, gpointer g_class)
{
    LaughIO *self = (LaughIO *)instance;

    self->priv = LAUGH_IO_GET_PRIVATE (self);

    self->priv->uri = NULL;
    self->priv->mime = NULL;
}

GType laugh_io_get_type (void)
{
    static GType type = 0;
    if (type == 0) {
        static const GTypeInfo info = {
            sizeof (LaughIOClass),
            NULL,   /* base_init */
            NULL,   /* base_finalize */
            (GClassInitFunc) laugh_io_class_init,   /* class_init */
            NULL,   /* class_finalize */
            NULL,   /* class_data */
            sizeof (LaughIO),
            0,      /* n_preallocs */
            laugh_io_instance_init    /* instance_init */
        };
        type = g_type_register_static (G_TYPE_OBJECT,
                "LaughIOType",
                &info, 0);
    }
    return type;
}

gchar *laugh_io_uri_resolve (const gchar *uri, const gchar *rel)
{
    gchar *new_uri;
    gboolean have_proto;

    if (!uri)
        return NULL;

    have_proto = !!g_strstr_len (uri, 15, ":/");

    if (!have_proto && rel && strncmp (uri, "data:", 5)) {
        GFile *file = g_file_new_for_uri (rel);
        GFile *path = g_file_get_parent (file);
        GFile *abs = g_file_resolve_relative_path (path ? path : file, uri);
        new_uri = g_file_get_uri (abs);
        g_object_unref (file);
        if (path)
            g_object_unref (path);
        g_object_unref (abs);
    } else if ((uri[0] == '/' || !have_proto) &&
            strncmp (uri, "data:", 5)) {
        GFile *file = g_file_new_for_path (uri);
        new_uri = g_file_get_uri (file);
        g_object_unref (file);
    } else {
        new_uri = g_strdup (uri);
    }
    return new_uri;
}

LaughIO *laugh_io_new (const gchar *uri, const gchar *rel)
{
    LaughIO *laughio;

    if (!uri)
        return NULL;

    laughio = LAUGH_IO (g_object_new (LAUGH_TYPE_IO, NULL));
    laughio->priv->uri = laugh_io_uri_resolve (uri, rel);

    return laughio;
}

static
void _laugh_io_read_async (GObject *source, GAsyncResult *res, LaughIO *io)
{
    LaughIOPrivate *priv = io->priv;
    if (priv->cancellable) {
        gsize sz = g_input_stream_read_finish (priv->fin, res, NULL);
        if (sz > 0) {
            gsize grow = priv->buf_size < 4096 ? 4096 : 8192;
            g_object_ref (G_OBJECT (io));

            if (!priv->buf_pos) {
                gboolean b;
                gchar *mt = g_content_type_guess (NULL, priv->buffer, sz, &b);
                if (mt) {
                    priv->mime = mt;
                    g_signal_emit (io, laugh_io_signals[MIME], 0, priv->mime);
                }
            }
            if (priv->cancellable) {
                priv->buf_size += grow;
                priv->buffer = g_try_realloc (priv->buffer, priv->buf_size);
                priv->buf_pos += sz;
                g_input_stream_read_async (priv->fin,
                        priv->buffer + priv->buf_pos,
                        grow-1, G_PRIORITY_DEFAULT, priv->cancellable,
                        (GAsyncReadyCallback) _laugh_io_read_async, io);
            }
            g_object_unref (G_OBJECT (io));
        } else if (sz == 0) {
            char *s = (char *)priv->buffer;
            s[priv->buf_pos] = 0;
            _laugh_io_clear_gio (priv, FALSE);
            g_signal_emit (io, laugh_io_signals[COMPLETED], 0,
                    priv->buf_pos, priv->buffer);
        } else {
            _laugh_io_clear_gio (priv, TRUE);
            g_signal_emit (io, laugh_io_signals[COMPLETED], 0, 0, NULL);
        }
    }
}

static
void _laugh_io_open_async (GObject *source, GAsyncResult *res, LaughIO *io)
{
    LaughIOPrivate *priv = io->priv;
    if (priv->cancellable) {
        priv->fin = (GInputStream *) g_file_read_finish (priv->file, res, NULL);
        if (priv->fin) {
            priv->buffer = g_malloc (300);
            priv->buf_size = 300;
            priv->buf_pos = 0;
            g_input_stream_read_async (priv->fin,
                    priv->buffer, priv->buf_size -1,
                    G_PRIORITY_DEFAULT, priv->cancellable,
                    (GAsyncReadyCallback) _laugh_io_read_async, io);
        } else {
            _laugh_io_clear_gio (priv, TRUE);
            g_signal_emit (io, laugh_io_signals[COMPLETED], 0, 0, NULL);
        }
    }
}

gboolean laugh_io_open (LaughIO *io)
{
    LaughIOPrivate *priv;

    g_return_if_fail (LAUGH_IS_IO (io));

    priv = io->priv;

    if (!strncmp (priv->uri, "data:", 5)) {
        const char *p = strchr (priv->uri + 5, ',');
        if (p) {
            g_object_ref (G_OBJECT (io));

            /*TODO handle encodings  */
            priv->mime = g_strdup ("text/plain");
            g_signal_emit (io, laugh_io_signals[MIME], 0, priv->mime);

            priv->buffer = *p ? g_uri_unescape_string (p + 1, "") : "";
            priv->buf_pos = strlen ((const char *) priv->buffer);
            g_signal_emit (io, laugh_io_signals[COMPLETED], 0,
                    priv->buf_pos, priv->buffer);

            g_object_unref (G_OBJECT (io));
        } else {
            g_signal_emit (io, laugh_io_signals[COMPLETED], 0, 0, NULL);
        }
    } else {
        priv->file = g_file_new_for_uri (priv->uri);
        priv->cancellable = g_cancellable_new ();
        g_file_read_async (priv->file, G_PRIORITY_DEFAULT,
                priv->cancellable,
                (GAsyncReadyCallback) _laugh_io_open_async, io);
    }

    return TRUE;
}

void laugh_io_cancel (LaughIO *io)
{
    g_return_if_fail (LAUGH_IS_IO (io));

    _laugh_io_clear_gio (io->priv, TRUE);
}

const gchar *laugh_io_get_uri (LaughIO *io)
{
    g_return_if_fail (LAUGH_IS_IO (io));

    return io->priv->uri;
}

/*#define LAUGH_IO_TEST*/
#ifdef LAUGH_IO_TEST
/* gcc laugh-io.c -o iotest `pkg-config --libs --cflags gio-2.0 glib-2.0 gthread-2.0` -DLAUGH_IO_TEST*/
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>

static GMainLoop *mainloop;
static int output_fd;
static const char *accept_mime;

void mime_type_callback (LaughIO *laugh_io, const gchar *mime, gpointer d)
{
    g_printf ("cb mime: %s\n", mime);
    if (accept_mime && strcmp (mime, accept_mime)) {
        laugh_io_cancel (laugh_io);
        g_main_loop_quit (mainloop);
    }
}

void completed_callback (LaughIO *laugh_io, gsize sz, gpointer data, gpointer d)
{
    g_printf ("cb completed: %u\n", sz);
    write (output_fd, data, sz);
    g_main_loop_quit (mainloop);
}

int main (int argc, char **argv)
{
    LaughIO *l;

    if (argc < 3) {
        g_printerr ("usage %s uri outputfile <accept-mime>\n", argv[0]);
        return 1;
    }

    output_fd = open (argv[2], O_CREAT | O_WRONLY, 0644);
    if (output_fd < 0) {
        g_printerr ("could not write to %s\n", argv[2]);
        return 1;
    }

    if (argc > 3)
        accept_mime = argv[3];

    g_thread_init (NULL);
    g_type_init ();

    l = laugh_io_new (argv[1]);
    g_printf ("uri: %s\n", laugh_io_get_uri (l));

    g_signal_connect (G_OBJECT (l), "mime-type", (GCallback)mime_type_callback, 0);
    g_signal_connect (G_OBJECT (l), "completed", (GCallback)completed_callback, 0);
    laugh_io_open (l);

    mainloop = g_main_loop_new (NULL, TRUE);
    g_main_loop_run (mainloop);

    g_object_unref (G_OBJECT (l));
    close (output_fd);

    return 0;
}

#endif
