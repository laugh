/*
 * Laugh.
 *
 * An glib SMIL library.
 *
 * Authored By Koos Vriezen  <koos.vriezen@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "laugh-dom.h"

#ifndef _LAUGH_TIMING_H
#define _LAUGH_TIMING_H

G_BEGIN_DECLS

#define LAUGH_TYPE_TIMING_CONTAINER laugh_timing_container_get_type()

#define LAUGH_TIMING_CONTAINER(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  LAUGH_TYPE_TIMING_CONTAINER, LaughTimingContainer))

#define LAUGH_TYPE_TIMING_CONTAINER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  LAUGH_TYPE_TIMING_CONTAINER, LaughTimingContainerClass))

#define LAUGH_IS_TIMING_CONTAINER(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  LAUGH_TYPE_TIMING_CONTAINER))

#define LAUGH_IS_TIMING_CONTAINER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  LAUGH_TYPE_TIMING_CONTAINER))

#define LAUGH_TIMING_CONTAINER_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  LAUGH_TYPE_TIMING_CONTAINER, LaughTimingContainerClass))

typedef struct _LaughTimingContainer        LaughTimingContainer;
typedef struct _LaughTimingContainerClass   LaughTimingContainerClass;
typedef struct _LaughTimingContainerPrivate LaughTimingContainerPrivate;
typedef enum _LaughTimingType LaughTimingType;
typedef struct _LaughTiming LaughTiming;
typedef struct _LaughRoleTiming LaughRoleTiming;

enum _LaughTimingType
{
    LaughTimingUnknown = -1,
    LaughTimingTime = 0, LaughTimingIndefinite, LaughTimingMedia,
    LaughTimingActivated, LaughTimingInbounds, LaughTimingOutbounds,
    LaughTimingEndSync, LaughTimingStartSync
};

struct _LaughTiming
{
    LaughTimingType type;
    int offset;
    gulong handler_id;
    gulong timeout_id;
    gchar *element_id;
    LaughNode *element;
};

struct _LaughRoleTiming
{
    LaughRole role;
    LaughNode *node;

    /*TODO: begin and end should support multiple events, make a list*/
    LaughTiming *begin;
    LaughTiming *dur;
    LaughTiming *end;

    gboolean active;
    gulong start_time_stamp;
    gulong stop_time_stamp;

    GSList *sub_segments;
    LaughRoleTiming *parent;
};

struct _LaughTimingContainer
{
    LaughNode parent;

    /*< public >*/
    LaughRoleTiming *timing_role;

    /*< private >*/
    LaughTimingContainerPrivate *priv;
};

struct _LaughTimingContainerClass
{
    LaughNodeClass parent_class;
};

GType laugh_timing_container_get_type (void) G_GNUC_CONST;

LaughTiming *laugh_timing_new ();

LaughTiming *laugh_timing_new_from_string (const gchar *value);

LaughNode *laugh_timing_container_new (LaughDocument *doc, LaughNodeTagId id,
                                       GHashTable *attributes);

void laugh_timing_delete (LaughTiming *timing);

LaughRoleTiming *laugh_timing_role_new (LaughNode *node);

void laugh_timing_role_delete (LaughRoleTiming *segment);

int laugh_timing_set_value (LaughTiming *timing, const gchar *value);

void laugh_timing_role_child_add (LaughRoleTiming *p, LaughRoleTiming *seg);

gboolean laugh_timing_setting_set_attribute (LaughRoleTiming *segment,
                LaughNodeAttributeId att, const gchar *val);

gboolean laugh_timing_setting_start (LaughRoleTiming *segment);

void laugh_timing_notify (LaughRoleTiming *segment, gulong handler_id);

void laugh_timing_setting_stop (LaughRoleTiming *segment);

void laugh_timing_setting_jump (LaughNode *target);

G_END_DECLS

#endif
