/*
 * Laugh.
 *
 * An glib SMIL library.
 *
 * Authored By Koos Vriezen  <koos.vriezen@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/**
 * SECTION:laugh-linking
 * @short_description: a/anchor/area linking classes.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "laugh-linking.h"
#include "laugh-layout.h"
#include "laugh-media.h"
#include "laugh-io.h"

#include <string.h>
#include <glib/gprintf.h>

#define LAUGH_LINKING_GET_PRIVATE(obj) \
(G_TYPE_INSTANCE_GET_PRIVATE ((obj), LAUGH_TYPE_LINKING, LaughLinkingPrivate))

struct _LaughLinkingPrivate
{
    LaughSize *coords;
    gchar *href;
};

static gpointer laugh_linking_parent_class = ((void *)0);

static void laugh_linking_finalize (GObject *object)
{
    G_OBJECT_CLASS (laugh_linking_parent_class)->finalize (object);
}

static void laugh_linking_dispose (GObject *object)
{
    LaughLinking *self = LAUGH_LINKING (object);
    LaughLinkingPrivate *priv = self->priv;

    if (priv->coords)
        g_free (priv->coords);
    if (priv->href)
        g_free (priv->href);

    G_OBJECT_CLASS (laugh_linking_parent_class)->dispose (object);
}

static gboolean _laugh_linking_href_timeout (LaughNode *node)
{
    LaughLinking *self = (LaughLinking *) node;
    gchar *uri = g_strdup (self->priv->href);

    laugh_document_set_uri (node->document, uri);

    laugh_document_open (node->document, NULL, -1, -1);

    g_free (uri);

    return FALSE;
}

static void
_laugh_linking_target_activated (LaughNode *node, gint x, gint y, LaughNode *link)
{
    LaughLinking *self = (LaughLinking *) link;
    LaughLinkingPrivate *priv = self->priv;
    const gchar *href;
    LaughNode *target = NULL;

    if (priv->coords && priv->coords[1].is_set) {
        LaughRoleDisplay *reg_dpy;
        guint w, h;

        reg_dpy = (LaughRoleDisplay *) laugh_node_role_get (node, LaughRoleTypeDisplay);
        if (!reg_dpy)
            return;

        clutter_actor_get_size (reg_dpy->actor, &w, &h);
        if (w == 0)
            return;

        if (laugh_size_get (priv->coords, w) > x ||
                laugh_size_get (priv->coords + 1, h) > y)
            return;
        if (priv->coords[3].is_set &&
                (laugh_size_get (priv->coords + 2, w) < x ||
                 laugh_size_get (priv->coords + 3, h) < y))
            return;
    }

    href = laugh_node_get_attribute (link, LaughAttrIdHRef);
    g_printf ("link activated %d %d %s\n", x, y, href);
    if (href && '#' == href[0]) {
        target = laugh_document_get_element_by_id (node->document,
                laugh_document_id_from_string (node->document, href + 1));
        if (target)
            laugh_timing_setting_jump (target);
        else
            g_printerr ("linking jump target %s not found\n", href ? href : "");
    } else if (href) {
        priv->href = g_strdup (href);
        g_timeout_add (0, _laugh_linking_href_timeout, self);
    }
}

static void
_laugh_linking_init (LaughNode *node, LaughInitializer *initializer)
{
    LaughNode *target = NULL;
    LaughNode *child;
    const gchar *target_id;

    if (node->attributes)
        g_hash_table_foreach (node->attributes, laugh_attributes_set, node);

    for (child = node->first_child; child; child = child->next_sibling)
        laugh_node_init (child, initializer);

    target_id = laugh_node_get_attribute (node, LaughAttrIdTarget);

    if (!target_id) {
        if (LaughTagIdA == node->id)
            target = node->first_child;
        else if (LaughTagIdAnchor == node->id || LaughTagIdArea == node->id)
            target = node->parent_node;
    } else {
        target = laugh_document_get_element_by_id (node->document,
                laugh_document_id_from_string (node->document, target_id));
    }
    if (target && !laugh_node_role_get (target, LaughRoleTypeMedia)) {
        g_printerr ("linking wrong target %s\n", laugh_tag_from_id (target->id));
        target = NULL;
    }
    if (!target) {
        g_printerr ("linking '%s' target %s not found\n",
                laugh_tag_from_id (node->id), target_id ? target_id : "");
        return;
    }
    g_signal_connect ((GObject *) target,
            "activated", G_CALLBACK (_laugh_linking_target_activated), node);
}

static void _laugh_linking_set_attribute (LaughNode *node,
        LaughNodeAttributeId att, const gchar *value, gpointer *undo)
{
    const gchar *val = value;
    LaughLinking *self = (LaughLinking *)node;
    LaughLinkingPrivate *priv = self->priv;

    LAUGH_TYPE_NODE_CLASS(laugh_linking_parent_class)->
        set_attribute(node, att, val, undo);

    if (!value && undo)
        val = *(const gchar **)undo;
    g_printf ("_laugh_linking_set_attribute %s=%s\n", laugh_attribute_from_id (att), val);

    switch (att) {
        case LaughAttrIdHRef:
            break;
        case LaughAttrIdTarget:
            break;
        case LaughAttrIdCoord: {
            int i;
            gchar **list;

            list = g_strsplit (val, ",", -1);

            if (self->priv->coords)
                g_free (self->priv->coords);

            priv->coords = g_new0 (LaughSize, 4);
            for (i = 0; list[i] && i < 4; ++i)
                laugh_size_set_string (priv->coords + i, list [i]);

            g_strfreev (list);
            break;
        }
        default:
            break;
    }
}

static void laugh_linking_class_init (LaughLinkingClass *klass)
{
    GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
    LaughNodeClass *node_class = (LaughNodeClass *) klass;

    laugh_linking_parent_class = g_type_class_peek_parent (klass);

    gobject_class->finalize = laugh_linking_finalize;
    gobject_class->dispose = laugh_linking_dispose;
    node_class->init = _laugh_linking_init;
    node_class->set_attribute = _laugh_linking_set_attribute;

    g_type_class_add_private (gobject_class, sizeof (LaughLinkingPrivate));
}

static
void laugh_linking_instance_init (GTypeInstance *instance, gpointer g_class)
{
    LaughLinking *self = (LaughLinking *)instance;

    self->priv = LAUGH_LINKING_GET_PRIVATE (self);
}

GType laugh_linking_get_type (void)
{
    static GType type = 0;
    if (type == 0) {
        static const GTypeInfo info = {
            sizeof (LaughLinkingClass),
            NULL,   /* base_init */
            NULL,   /* base_finalize */
            (GClassInitFunc) laugh_linking_class_init,   /* class_init */
            NULL,   /* class_finalize */
            NULL,   /* class_data */
            sizeof (LaughLinking),
            0,      /* n_preallocs */
            laugh_linking_instance_init    /* instance_init */
        };
        type = g_type_register_static (LAUGH_TYPE_NODE,
                "LaughLinkingType",
                &info, 0);
    }
    return type;
}

LaughNode *laugh_linking_new (LaughDocument *doc, LaughNodeTagId id,
        GHashTable *attributes)
{
    LaughNode *n = (LaughNode *) g_object_new (LAUGH_TYPE_LINKING, NULL);

    laugh_node_base_construct (doc, n, id, attributes);

    return n;
}
