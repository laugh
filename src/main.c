/*
 * Laugh.
 *
 * An glib SMIL library.
 *
 * Authored By Koos Vriezen  <koos.vriezen@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "laugh-actor.h"

#include <glib/gprintf.h>
#include <clutter/clutter-main.h>
#include <clutter/clutter-container.h>
#include <clutter/clutter-stage.h>

void loaded_callback (LaughActor *actor, gpointer d)
{
    (void) actor;
    (void) d;

    g_printf ("cb loaded\n");
}

void finished_callback (LaughActor *actor, gpointer d)
{
    (void) actor;
    (void) d;

    g_printf ("cb stopped\n");
}

int main (int argc, char **argv)
{
    ClutterActor *laugh;
    ClutterActor *stage;
    guint width, height;

    if (argc < 2) {
        g_printerr ("usage %s uri\n", argv[0]);
        return 1;
    }

    g_thread_init (NULL);
    clutter_gst_init (&argc, &argv);

    stage = clutter_stage_get_default ();
    clutter_actor_get_size (stage, &width, &height);

    laugh = laugh_actor_new ();
    laugh_actor_uri_set (LAUGH_ACTOR (laugh), argv[1]);

    g_signal_connect (G_OBJECT (laugh), "notify::loaded",
            (GCallback) loaded_callback, (gpointer) 0);
    g_signal_connect (G_OBJECT (laugh), "notify::finished",
            (GCallback) finished_callback, (gpointer) 0);

    clutter_actor_set_size (laugh, width, height);
    clutter_container_add_actor (CLUTTER_CONTAINER (stage), laugh);

    laugh_actor_start (LAUGH_ACTOR (laugh));
    clutter_actor_show (laugh);

    clutter_actor_show_all (stage);

    clutter_main ();

    return 0;
}
