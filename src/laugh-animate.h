/*
 * Laugh.
 *
 * An glib SMIL library.
 *
 * Authored By Koos Vriezen  <koos.vriezen@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef _LAUGH_ANIMATE_H
#define _LAUGH_ANIMATE_H

#include "laugh-dom.h"

G_BEGIN_DECLS

#define LAUGH_TYPE_ANIMATE laugh_animate_get_type()

#define LAUGH_ANIMATE(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  LAUGH_TYPE_ANIMATE, LaughAnimate))

#define LAUGH_TYPE_ANIMATE_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  LAUGH_TYPE_ANIMATE, LaughAnimateClass))

#define LAUGH_IS_ANIMATE(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  LAUGH_TYPE_ANIMATE))

#define LAUGH_IS_ANIMATE_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  LAUGH_TYPE_ANIMATE))

#define LAUGH_ANIMATE_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  LAUGH_TYPE_ANIMATE, LaughAnimateClass))

typedef struct _LaughAnimate        LaughAnimate;
typedef struct _LaughAnimateClass   LaughAnimateClass;
typedef struct _LaughAnimatePrivate LaughAnimatePrivate;

struct _LaughAnimate
{
    LaughNode parent;

    /*< private >*/
    LaughAnimatePrivate *priv;
};

struct _LaughAnimateClass
{
    LaughNodeClass parent_class;
};

GType laugh_animate_get_type (void) G_GNUC_CONST;

LaughNode *laugh_animate_new (LaughDocument *doc, LaughNodeTagId id,
                                       GHashTable *attributes);

G_END_DECLS

#endif
