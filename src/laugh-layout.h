/*
 * Laugh.
 *
 * An glib SMIL library.
 *
 * Authored By Koos Vriezen  <koos.vriezen@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef _LAUGH_LAYOUT_H
#define _LAUGH_LAYOUT_H

#include "laugh-dom.h"
#include <clutter/clutter-actor.h>

G_BEGIN_DECLS

#define LAUGH_TYPE_LAYOUT laugh_layout_get_type()

#define LAUGH_LAYOUT(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  LAUGH_TYPE_LAYOUT, LaughLayout))

#define LAUGH_TYPE_LAYOUT_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  LAUGH_TYPE_LAYOUT, LaughLayoutClass))

#define LAUGH_IS_LAYOUT(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  LAUGH_TYPE_LAYOUT))

#define LAUGH_IS_LAYOUT_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  LAUGH_TYPE_LAYOUT))

#define LAUGH_LAYOUT_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  LAUGH_TYPE_LAYOUT, LaughLayoutClass))

typedef struct _LaughLayout        LaughLayout;
typedef struct _LaughLayoutClass   LaughLayoutClass;
typedef struct _LaughLayoutPrivate LaughLayoutPrivate;
typedef struct _LaughSize LaughSize;
typedef struct _LaughSizeSetting LaughSizeSetting;
typedef struct _LaughRoleDisplay LaughRoleDisplay;
typedef struct _LaughRoleDisplayGroup LaughRoleDisplayGroup;

struct _LaughSize
{
    float perc_size;
    float abs_size;
    gboolean is_set;
};

struct _LaughSizeSetting
{
    LaughSize left;
    LaughSize top;
    LaughSize width;
    LaughSize height;
    LaughSize right;
    LaughSize bottom;
};

struct _LaughRoleDisplay
{
    LaughRole role;
    ClutterActor *actor;
};

struct _LaughRoleDisplayGroup
{
    LaughRole role;
    ClutterActor *actor;
};

struct _LaughLayout
{
    LaughNode parent;

    /*< public >*/
    LaughSizeSetting size_setting;

    /*< private >*/
    LaughLayoutPrivate *priv;
};

struct _LaughLayoutClass
{
    LaughNodeClass parent_class;
};

GType laugh_layout_get_type (void) G_GNUC_CONST;

void laugh_size_set_string (LaughSize *size, const gchar *value);

float laugh_size_get (LaughSize *size, float relative_to);

gboolean laugh_size_setting_set_attribute (LaughSizeSetting *sizes,
                        LaughNodeAttributeId att, const gchar *val);

void laugh_size_setting_get (LaughSizeSetting *sizes, float pw, float ph,
                float *x, float *y, float *w, float *h);

LaughNode *laugh_layout_new (LaughDocument *doc, LaughNodeTagId id,
        GHashTable *attributes);

G_END_DECLS

#endif
