/*
 * Laugh.
 *
 * An glib SMIL library.
 *
 * Authored By Koos Vriezen  <koos.vriezen@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef _LAUGH_LINKING_H
#define _LAUGH_LINKING_H

#include "laugh-dom.h"

G_BEGIN_DECLS

#define LAUGH_TYPE_LINKING laugh_linking_get_type()

#define LAUGH_LINKING(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  LAUGH_TYPE_LINKING, LaughLinking))

#define LAUGH_TYPE_LINKING_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  LAUGH_TYPE_LINKING, LaughLinkingClass))

#define LAUGH_IS_LINKING(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  LAUGH_TYPE_LINKING))

#define LAUGH_IS_LINKING_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  LAUGH_TYPE_LINKING))

#define LAUGH_LINKING_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  LAUGH_TYPE_LINKING, LaughLinkingClass))

typedef struct _LaughLinking        LaughLinking;
typedef struct _LaughLinkingClass   LaughLinkingClass;
typedef struct _LaughLinkingPrivate LaughLinkingPrivate;

struct _LaughLinking
{
    LaughNode parent;

    /*< private >*/
    LaughLinkingPrivate *priv;
};

struct _LaughLinkingClass
{
    LaughNodeClass parent_class;
};

GType laugh_linking_get_type (void) G_GNUC_CONST;

LaughNode *laugh_linking_new (LaughDocument *doc, LaughNodeTagId id,
                                       GHashTable *attributes);

G_END_DECLS

#endif
