/*
 * Laugh.
 *
 * An glib SMIL library.
 *
 * Authored By Koos Vriezen  <koos.vriezen@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/**
 * SECTION:laugh-media
 * @short_description: text/img/brush/ref/audio/video media classes.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "laugh-marshal.h"
#include "laugh-media.h"
#include "laugh-layout.h"
#include "laugh-io.h"

#include <string.h>
#include <glib/gprintf.h>
#include <clutter/clutter-label.h>
#include <clutter/clutter-group.h>
#include <clutter/clutter-rectangle.h>
#include <clutter/clutter-texture.h>
#include <clutter-gst/clutter-gst.h>
#ifdef HAVE_WEBKIT_CLUTTER
#include <webkit/webkit.h>
#endif

#define LAUGH_MEDIA_GET_PRIVATE(obj) \
(G_TYPE_INSTANCE_GET_PRIVATE ((obj), LAUGH_TYPE_MEDIA, LaughMediaPrivate))

enum {
    ACTIVATED,

    LAST_SIGNAL
};

typedef enum {
    LaughMediaTypeUnknown = 0,
    LaughMediaTypeAudio,
    LaughMediaTypeImage,
    LaughMediaTypeText,
#ifdef HAVE_WEBKIT_CLUTTER
    LaughMediaTypeTextHTML,
#endif
    LaughMediaTypeVideo
} LaughMediaType;

struct _LaughMediaPrivate
{
    LaughRoleDisplay *display_role;
    LaughRoleTiming *timing_role;
    LaughMediaType type;
    LaughSizeSetting size_setting;
    LaughNode *region;
    LaughIO *io;
    gpointer *data;
    guint intrinsic_width;
    guint intrinsic_height;
    gchar *uri;
};

static gpointer laugh_media_parent_class = ((void *)0);
static guint laugh_media_signals[LAST_SIGNAL] = { 0, };
static LaughRole laugh_media_media_role;

static void laugh_media_finalize (GObject *object)
{
    G_OBJECT_CLASS (laugh_media_parent_class)->finalize (object);
}

static void laugh_media_dispose (GObject *object)
{
    LaughMedia *self = LAUGH_MEDIA(object);
    LaughRoleTiming *role = self->priv->timing_role;

    if (self->priv->display_role) {
        clutter_actor_destroy (self->priv->display_role->actor);
        g_free (self->priv->display_role);
    }
    if (self->priv->data)
        switch (self->priv->type) {
            case LaughMediaTypeText:
                g_free (self->priv->data);
                break;
            case LaughMediaTypeImage:
            case LaughMediaTypeAudio:
                g_object_unref (self->priv->data);
                break;
            default:
                break;
        }
    if (self->priv->uri)
        g_free (self->priv->uri);

    G_OBJECT_CLASS (laugh_media_parent_class)->dispose (object);

    laugh_timing_role_delete (role);
}

static void
_laugh_media_init (LaughNode *node, LaughInitializer *initializer)
{
    LaughMedia *self = (LaughMedia *) node;
    LaughNode *child;
    LaughRoleTiming *role = self->priv->timing_role;
    LaughRoleTiming *parent_timing_role = initializer->parent_segment;

    if (node->attributes)
        g_hash_table_foreach (node->attributes, laugh_attributes_set, node);

    if (self->priv->io) {
        initializer->init_pending = g_slist_append (initializer->init_pending,
                node);
        g_signal_connect (G_OBJECT (node), "initialized",
                (GCallback) initializer->initialized, initializer);
    }

    laugh_timing_role_child_add (initializer->parent_segment, role);

    initializer->parent_segment = role;

    for (child = node->first_child; child; child = child->next_sibling)
        laugh_node_init (child, initializer);

    initializer->parent_segment = parent_timing_role;
}

void _lauch_media_mime_type (LaughIO *io, const gchar *mime, gpointer d)
{
    LaughMedia *media = LAUGH_MEDIA (d);
    LaughNode *node = (LaughNode *) media;

    g_printf ("lauch_media_mime_type: %s\n", mime);
#ifdef HAVE_WEBKIT_CLUTTER
    if (!strncmp (mime, "text/html", 9)) {
        media->priv->type = LaughMediaTypeTextHTML;
    } else
#endif
    if (!strncmp (mime, "text/", 5)) {
        media->priv->type = LaughMediaTypeText;
    } else if (!strncmp (mime, "image/", 6)) {
        media->priv->type = LaughMediaTypeImage;
    } else if (!strncmp (mime, "audio/", 6)) {
        media->priv->type = LaughMediaTypeAudio;
    } else if (!strncmp (mime, "video/", 6)) {
        media->priv->type = LaughMediaTypeVideo;
    } else if (!strcmp (mime, "application/octet-stream")) {
        if (LaughTagIdAudio)
            media->priv->type = LaughMediaTypeAudio;
        else if (LaughTagIdVideo)
            media->priv->type = LaughMediaTypeVideo;
    }
    if (LaughMediaTypeUnknown == media->priv->type)
        g_printerr ("%s mimetype '%s' not (yet) supported\n",
                laugh_node_get_attribute (node, LaughAttrIdSrc), mime);
    if (LaughMediaTypeImage != media->priv->type &&
            LaughMediaTypeText != media->priv->type) {
        if (LaughMediaTypeUnknown != media->priv->type)
            media->priv->uri = g_strdup (laugh_io_get_uri (io));
        laugh_io_cancel (io);
        media->priv->io = NULL;

        g_object_unref (G_OBJECT (io));

        laugh_node_base_emit_initialized (node);
    }
}

static void
_lauch_media_completed (LaughIO *io, gsize sz, gpointer data, gpointer d)
{
    LaughMedia *media = LAUGH_MEDIA (d);
    LaughNode *node = (LaughNode *) media;
    LaughMediaPrivate *priv = media->priv;

    g_printf ("_lauch_media_completed %s: %u\n", laugh_io_get_uri (io), sz);
    if (data) {
        switch (priv->type) {
            case LaughMediaTypeText:
                priv->data = data;
                g_printf ("text: %s\n", (const char *)data);
                break;
            case LaughMediaTypeImage: {
                GdkPixbufLoader *loader = gdk_pixbuf_loader_new ();
                if (gdk_pixbuf_loader_write (loader,
                            (const guchar *) data, sz, NULL) &&
                        gdk_pixbuf_loader_close (loader, NULL)) {
                    GdkPixbuf *pix = gdk_pixbuf_loader_get_pixbuf (loader);
                    g_printf ("image:\n");
                    g_object_ref (pix);
                    priv->data = pix;
                    priv->intrinsic_width = gdk_pixbuf_get_width (pix);
                    priv->intrinsic_height = gdk_pixbuf_get_height (pix);
                }
                g_object_unref (loader);
                g_free (data);
                break;
            }
            case LaughMediaTypeAudio:
                break;
            case LaughMediaTypeVideo:
                /*TODO: determine sizes*/
                break;
            default:
                g_free (data);
                break;
        }
    }
    g_object_unref (G_OBJECT (io));
    priv->io = NULL;

    laugh_node_base_emit_initialized (node);
}

static void _laugh_media_actor_position (LaughNode *node)
{
    LaughMedia *self = (LaughMedia *)node;
    LaughMediaPrivate *priv = self->priv;
    LaughRoleDisplay *region_display;
    guint pw, ph;
    float sx, sy, sw, sh;
    const gchar *fit;

    region_display =
        (LaughRoleDisplay *) laugh_node_role_get (priv->region, LaughRoleTypeDisplay);
    clutter_actor_get_size (region_display->actor, &pw, &ph);
    laugh_size_setting_get (&priv->size_setting, pw, ph, &sx, &sy, &sw, &sh);
    fit = laugh_node_get_attribute(node, LaughAttrIdFit);
    if (!fit)
        fit = laugh_node_get_attribute (priv->region, LaughAttrIdFit);
    if (priv->intrinsic_width > 0 && priv->intrinsic_height > 0) {
        if (!fit || !strcmp (fit, "hidden") || !strcmp (fit, "scroll")) {
            sw = priv->intrinsic_width;
            sh = priv->intrinsic_height;
        } else if (!strcmp (fit, "meet")) {
            float iasp = 1.0 * priv->intrinsic_width / priv->intrinsic_height;
            float rasp = 1.0 * sw / sh;
            if (iasp > rasp)
                sh = priv->intrinsic_height * sw / priv->intrinsic_width;
            else
                sw = priv->intrinsic_width * sh / priv->intrinsic_height;
        } else if (!strcmp (fit, "slice")) {
            float iasp = 1.0 * priv->intrinsic_width / priv->intrinsic_height;
            float rasp = 1.0 * sw / sh;
            if (iasp > rasp)
                sw = priv->intrinsic_width * sh / priv->intrinsic_height;
            else
                sh = priv->intrinsic_height * sw / priv->intrinsic_width;
        }
    }
    clutter_actor_set_size (priv->display_role->actor, sw, sh);
    clutter_actor_set_position (priv->display_role->actor, sx, sy);
}

static void _laugh_media_set_attribute (LaughNode *node,
        LaughNodeAttributeId att, const gchar *value, gpointer *undo)
{
    const gchar *val = value;
    LaughMedia *self = (LaughMedia *)node;
    LaughMediaPrivate *priv = self->priv;

    LAUGH_TYPE_NODE_CLASS(laugh_media_parent_class)->
        set_attribute(node, att, val, undo);

    if (!value && undo)
        val = *(const gchar **)undo;
    g_printf ("_laugh_media_set_attribute %s=%s\n", laugh_attribute_from_id (att), val);

    if (laugh_timing_setting_set_attribute (priv->timing_role, att, val)) {
    } else if (laugh_size_setting_set_attribute (&self->priv->size_setting, att, value) ||
            LaughAttrIdFit == att) {
        if (priv->region && priv->display_role)
            _laugh_media_actor_position (node);
    } else {
        switch (att) {
            case LaughAttrIdSrc:
                if (val) {
                    priv->io = laugh_io_new (val,
                            laugh_document_get_uri (node->document));

                    g_signal_connect (G_OBJECT (priv->io), "mime-type",
                            (GCallback) _lauch_media_mime_type, (gpointer) node);
                    g_signal_connect (G_OBJECT (priv->io), "completed",
                            (GCallback) _lauch_media_completed, (gpointer) node);

                    laugh_io_open (priv->io);
                }
                break;
            default:
                break;
        }
    }
}

static void _laugh_media_region_destroyed (gpointer data, GObject *obj)
{
    LaughMediaPrivate *priv = (LaughMediaPrivate *) data;
    (void) obj;

    priv->region = NULL;
    g_free (priv->display_role);
    priv->display_role = NULL;
}

static void _lauch_media_actor_destroyed (ClutterActor *actor, gpointer data)
{
    LaughMedia *self = LAUGH_MEDIA(data);

    g_free (self->priv->display_role);
    self->priv->display_role = NULL;
}

static void _laugh_media_eos (ClutterMedia *media, LaughNode *node)
{
    LaughMedia *self = (LaughMedia *)node;
    LaughRoleTiming *role = self->priv->timing_role;

    if (LaughStateBegun == node->state &&
            role->dur &&
            LaughTimingMedia == role->dur->type) {
        laugh_timing_notify (role, role->dur->handler_id);
    }
}

static
void _laugh_media_size_change (ClutterTexture *tex, gint w, gint h, LaughMedia *self)
{
    g_printf ("size_change %d %d\n", w, h);
}

static gboolean
_lauch_media_actor_event (ClutterActor *actor, ClutterEvent *event, LaughMedia *self)
{
    ClutterUnit x, y;
    clutter_actor_transform_stage_point (actor,
            CLUTTER_UNITS_FROM_INT (event->button.x),
            CLUTTER_UNITS_FROM_INT (event->button.y),
            &x, &y);
    g_signal_emit (self, laugh_media_signals[ACTIVATED],
            0, CLUTTER_UNITS_TO_INT(x), CLUTTER_UNITS_TO_INT(y));
    g_printf ("button_press %d %d\n", CLUTTER_UNITS_TO_INT(x), CLUTTER_UNITS_TO_INT(y));

    return FALSE;
}

static void _laugh_media_display_role_new (LaughNode *node)
{
    LaughMedia *self = (LaughMedia *)node;
    LaughRoleDisplay *display_role;

    display_role = g_new0 (LaughRoleDisplay, 1);
    display_role->role.type = LaughRoleTypeDisplay;

    self->priv->display_role = display_role;
}

static void _laugh_media_start (LaughNode *node)
{
    LaughMedia *self = (LaughMedia *)node;
    LaughMediaPrivate *priv = self->priv;
    LaughNode *region;
    ClutterMedia *media = NULL;
    const gchar *region_id = laugh_node_get_attribute(node, LaughAttrIdRegion);

    if (!region_id) {
        g_printerr ("'%s' default region not implemented\n",
                laugh_tag_from_id (node->id));
        return;
    }
    region = laugh_document_get_element_by_id (node->document,
            laugh_document_id_from_string (node->document, region_id));
    if (region && LAUGH_IS_LAYOUT(region)) {
        priv->region = region;
    } else {
        g_printerr ("start '%s' region \"%s\" not found\n",
                laugh_tag_from_id (node->id), region_id);
        return;
    }
    g_object_weak_ref ((GObject *) region, _laugh_media_region_destroyed, priv);

    switch (priv->type) {
        case LaughMediaTypeText:
            if (priv->data) {
                _laugh_media_display_role_new (node);
                priv->display_role->actor = clutter_label_new_with_text (
                        "sans 10", (const char *)priv->data);
            }
            break;
#ifdef HAVE_WEBKIT_CLUTTER
        case LaughMediaTypeTextHTML:
            _laugh_media_display_role_new (node);
            priv->display_role->actor = webkit_web_view_new (50, 50);
            webkit_web_view_open ((WebKitWebView *)priv->display_role->actor, priv->uri);
            break;
#endif
        case LaughMediaTypeImage:
            if (priv->data) {
                _laugh_media_display_role_new (node);
                priv->display_role->actor = clutter_texture_new_from_pixbuf (
                        (GdkPixbuf *)priv->data);
            }
            break;
        case LaughMediaTypeAudio:
            media = (ClutterMedia *) clutter_gst_audio_new ();
            priv->data = (gpointer) media;
            break;
        case LaughMediaTypeVideo:
            _laugh_media_display_role_new (node);
            priv->display_role->actor = clutter_gst_video_texture_new ();
            media = (ClutterMedia *) priv->display_role->actor;
            g_object_set (G_OBJECT(priv->display_role->actor), "sync-size", FALSE, NULL);
            g_signal_connect (CLUTTER_TEXTURE(priv->display_role->actor),
                                        "size-change",
                                        G_CALLBACK (_laugh_media_size_change), self);
            break;
        default:
            break;
    }
    if (LaughMediaTypeAudio == priv->type || LaughMediaTypeVideo == priv->type) {
        LaughRoleTiming *role = self->priv->timing_role;
        if (!role->dur) {
            role->dur = laugh_timing_new ();
            role->dur->type = LaughTimingMedia;
        }
        role->dur->handler_id = g_signal_connect (G_OBJECT (media),
                "eos", (GCallback) _laugh_media_eos, self);

        g_printf ("playing gst %s\n", priv->uri);
        clutter_media_set_uri (media, priv->uri);
        clutter_media_set_playing (media, TRUE);
    }
    if (priv->display_role) {
        LaughRoleDisplayGroup *group = (LaughRoleDisplayGroup *)
            laugh_node_role_get (priv->region, LaughRoleTypeDisplayGroup);
        clutter_container_add_actor (CLUTTER_CONTAINER (group->actor), priv->display_role->actor);
        _laugh_media_actor_position (node);
        CLUTTER_ACTOR_SET_FLAGS (priv->display_role->actor, CLUTTER_ACTOR_REACTIVE);
        clutter_actor_show (priv->display_role->actor);
        g_signal_connect (G_OBJECT (priv->display_role->actor), "button-press-event",
                (GCallback) _lauch_media_actor_event, self);
        g_signal_connect (G_OBJECT (priv->display_role->actor), "destroy",
                (GCallback) _lauch_media_actor_destroyed, self);
    }
}

static void _laugh_media_stop (LaughNode *node)
{
    LaughMedia *self = (LaughMedia *)node;
    LaughMediaPrivate *priv = self->priv;

    switch (node->id) {
        case LaughTagIdAudio:
        case LaughTagIdVideo:
            if (priv->data) {
                ClutterMedia *m = CLUTTER_MEDIA (priv->data);
                clutter_media_set_playing (m, FALSE);
                g_object_unref (priv->data);
                priv->data = NULL;
            }
            break;
        default:
            break;
    }

    if (priv->region) {
        g_object_weak_unref ((GObject *) priv->region,
                _laugh_media_region_destroyed, priv);
        priv->region = NULL;
    }

    if (priv->display_role) {
        clutter_actor_destroy (priv->display_role->actor);
        g_free (priv->display_role);
        priv->display_role = NULL;
    }
}

static LaughRole *_laugh_media_role (LaughNode *node, LaughRoleType type)
{
    LaughMedia *self = (LaughMedia *) node;

    switch (type) {
        case LaughRoleTypeDisplay:
            return (LaughRole *) self->priv->display_role;
        case LaughRoleTypeMedia:
            return &laugh_media_media_role;
        case LaughRoleTypeTiming:
            return (LaughRole *) self->priv->timing_role;
        default:
            return NULL;
    }
}

static void laugh_media_class_init (LaughMediaClass *klass)
{
    GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
    LaughNodeClass *node_class = (LaughNodeClass *) klass;

    laugh_media_parent_class = g_type_class_peek_parent (klass);

    gobject_class->finalize = laugh_media_finalize;
    gobject_class->dispose = laugh_media_dispose;
    node_class->init = _laugh_media_init;
    node_class->set_attribute = _laugh_media_set_attribute;
    node_class->start = _laugh_media_start;
    node_class->stop = _laugh_media_stop;
    node_class->role = _laugh_media_role;

    laugh_media_signals[ACTIVATED] =
        g_signal_new ("activated",
                G_TYPE_FROM_CLASS (gobject_class),
                G_SIGNAL_RUN_LAST,
                G_STRUCT_OFFSET (LaughMediaClass, activated),
                NULL, NULL,
                laugh_marshal_VOID__INT_INT,
                G_TYPE_NONE, 2, G_TYPE_INT, G_TYPE_INT);

    g_type_class_add_private (gobject_class, sizeof (LaughMediaPrivate));
}

static
void laugh_media_instance_init (GTypeInstance *instance, gpointer g_class)
{
    LaughMedia *self = (LaughMedia *)instance;

    self->priv = LAUGH_MEDIA_GET_PRIVATE (self);

    self->priv->timing_role = laugh_timing_role_new ((LaughNode *)self);
}

GType laugh_media_get_type (void)
{
    static GType type = 0;
    if (type == 0) {
        static const GTypeInfo info = {
            sizeof (LaughMediaClass),
            NULL,   /* base_init */
            NULL,   /* base_finalize */
            (GClassInitFunc) laugh_media_class_init,   /* class_init */
            NULL,   /* class_finalize */
            NULL,   /* class_data */
            sizeof (LaughMedia),
            0,      /* n_preallocs */
            laugh_media_instance_init    /* instance_init */
        };
        type = g_type_register_static (LAUGH_TYPE_NODE,
                "LaughMediaType",
                &info, 0);
        laugh_media_media_role.type = LaughRoleTypeMedia;
    }
    return type;
}

LaughNode *laugh_media_new (LaughDocument *doc, LaughNodeTagId id,
        GHashTable *attributes)
{
    LaughNode *n = (LaughNode *)g_object_new(LAUGH_TYPE_MEDIA, NULL);

    laugh_node_base_construct (doc, n, id, attributes);

    return n;
}
