/*
 * Laugh.
 *
 * An glib SMIL library.
 *
 * Authored By Koos Vriezen  <koos.vriezen@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef _LAUGH_ACTOR_H
#define _LAUGH_ACTOR_H

#include <clutter/clutter-group.h>

G_BEGIN_DECLS

#define LAUGH_TYPE_ACTOR laugh_actor_get_type()

#define LAUGH_ACTOR(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  LAUGH_TYPE_ACTOR, LaughActor))

#define LAUGH_TYPE_ACTOR_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  LAUGH_TYPE_ACTOR, LaughActorClass))

#define LAUGH_IS_ACTOR(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  LAUGH_TYPE_ACTOR))

#define LAUGH_IS_ACTOR_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  LAUGH_TYPE_ACTOR))

#define LAUGH_ACTOR_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  LAUGH_TYPE_ACTOR, LaughActorClass))

typedef struct _LaughActor        LaughActor;
typedef struct _LaughActorClass   LaughActorClass;
typedef struct _LaughActorPrivate LaughActorPrivate;

struct _LaughActor
{
    ClutterGroup parent;

    /*< private >*/
    LaughActorPrivate *priv;
};

struct _LaughActorClass
{
    ClutterGroupClass parent_class;
};

GType laugh_actor_get_type (void) G_GNUC_CONST;

ClutterActor *laugh_actor_new ();

void laugh_actor_uri_set (LaughActor *actor, const gchar *uri);

void laugh_actor_start (LaughActor *actor);

G_END_DECLS

#endif
